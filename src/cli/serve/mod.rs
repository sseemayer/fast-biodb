mod api;
mod ui;

use std::{error::Error, path::PathBuf, str::FromStr, sync::Arc};

use anyhow::{Context, Result};

use poem::{
    listener::TcpListener,
    middleware::{Cors, NormalizePath, Tracing},
    EndpointExt, Route,
};
use poem_openapi::OpenApiService;
use structopt::StructOpt;
use tracing::{span, Level};

use crate::{
    cli::serve::api::{Api, AppState},
    format::{OutputFormat, ParseEntry, WriteEntry},
    VERSION,
};

#[derive(StructOpt)]
pub struct Serve<T>
where
    T: ParseEntry + WriteEntry + 'static,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err:
        std::fmt::Debug + std::fmt::Display + Send + Sync + Error,
{
    /// Database index to search in
    #[structopt(short = "d", long)]
    database: PathBuf,

    /// The URL to bind the API to
    #[structopt(short = "b", long, default_value = "127.0.0.1:3000")]
    bind_url: String,

    #[structopt(skip)]
    _marker: std::marker::PhantomData<T>,
}

impl<T> Serve<T>
where
    T: ParseEntry + WriteEntry + Send + Sync + 'static,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err:
        std::fmt::Debug + std::fmt::Display + Send + Sync + Error,
{
    pub fn run(&self) -> Result<()> {
        tracing_subscriber::fmt::init();

        let span = span!(Level::DEBUG, "search_setup").entered();

        let (_schema, index, reader, query_parser) =
            crate::search::load_index(&self.database).context("Loading database index")?;

        span.exit();

        let state = Arc::new(AppState {
            index,
            reader,
            query_parser,
        });
        let program_name = std::env::current_exe()
            .unwrap()
            .file_stem()
            .unwrap_or_default()
            .to_string_lossy()
            .to_string();

        let api_service =
            OpenApiService::new(Api::<T>::new(), program_name.clone(), VERSION.to_string())
                .server(format!("http://{}/api", self.bind_url));

        let swagger_ui = api_service.swagger_ui();
        let ui = crate::cli::serve::ui::AssetEndpoint {
            program_name,
            version: VERSION.to_string(),
            output_formats: T::OutputFormat::valid_formats()
                .iter()
                .map(|f| f.to_string())
                .collect(),
        };

        let (bind_host, api_path) = if let Some((h, p)) = self.bind_url.split_once('/') {
            (h, p)
        } else {
            (&self.bind_url[..], "")
        };

        let app = Route::new().nest(
            api_path,
            Route::new()
                .nest("/api", api_service)
                .nest("/api/swagger", swagger_ui)
                .nest("/", ui)
                .with(Tracing::default())
                .with(Cors::new())
                .with(NormalizePath::new(poem::middleware::TrailingSlash::Trim))
                .data(state),
        );

        tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()
            .unwrap()
            .block_on(async {
                poem::Server::new(TcpListener::bind(bind_host.to_string()))
                    .run(app)
                    .await
            })?;

        Ok(())
    }
}
