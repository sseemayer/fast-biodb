use std::{
    collections::{HashMap, HashSet},
    error::Error,
    fs::File,
    io::{Read, Seek, SeekFrom},
    str::FromStr,
    sync::Arc,
};

use poem::web::Data;

use poem_openapi::{
    param::Path,
    payload::{Binary, Json, PlainText},
    ApiResponse, Object, OpenApi,
};

use tantivy::{query::QueryParser, Index, IndexReader};

use tracing::{event, span, Level};

use crate::{
    format::{OutputFormat, ParseEntry, WriteEntry},
    search::EntryIterator,
};

pub struct AppState {
    pub index: Index,
    pub reader: IndexReader,
    pub query_parser: QueryParser,
}

type State = Arc<AppState>;

#[derive(Default)]
pub struct Api<T>
where
    T: ParseEntry + WriteEntry,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err:
        std::fmt::Debug + std::fmt::Display + Send + Sync + Error,
{
    _marker: std::marker::PhantomData<T>,
}

/// Retrieval of one or more entries
#[derive(Object)]
struct EntryRequest {
    /// List of entry identifiers
    ids: Vec<String>,
}

#[derive(Debug, ApiResponse)]
enum GetEntryResponse {
    #[oai(status = 200)]
    Ok(Binary<Vec<u8>>, #[oai(header = "Content-Type")] String),
    #[oai(status = 400)]
    BadOutputFormat(PlainText<String>),
}

/// Search for entries
#[derive(Object)]
struct SearchRequest {
    /// The search query
    #[oai(default)]
    query: String,

    /// The number of hits to return
    #[oai(default = "default_limit")]
    limit: usize,

    /// Offset the hit list by a number of entries. Used for pagination.
    offset: usize,
}

fn default_limit() -> usize {
    100
}

#[derive(Debug, ApiResponse)]
enum SearchResponse {
    /// Response for identifier list
    #[oai(status = 200)]
    Identifiers(Json<SearchHits>),

    /// Bad search query
    #[oai(status = 400)]
    BadQuery(PlainText<String>),
}

#[derive(Object, Debug)]
struct SearchHits {
    hits: Vec<ScoredHit>,
    total: usize,
}

#[derive(Object, Debug)]
struct ScoredHit {
    score: f32,
    identifier: String,
    description: String,
}

#[OpenApi]
impl<T> Api<T>
where
    T: ParseEntry + WriteEntry + Send + Sync + 'static,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err:
        std::fmt::Debug + std::fmt::Display + Send + Sync + Error,
{
    pub fn new() -> Self {
        Self {
            _marker: Default::default(),
        }
    }

    /// Get entries as raw data
    ///
    /// Entries will be returned as a JSON document, with keys corresponding to the requested
    /// identifiers, and values to the raw, unparsed entry data.
    #[oai(path = "/entry/raw", method = "post")]
    async fn get_entry_raw(
        &self,
        state: Data<&State>,
        data: Json<EntryRequest>,
    ) -> poem::Result<Json<HashMap<String, String>>> {
        let identifiers: HashSet<Vec<u8>> =
            data.ids.iter().map(|id| id.as_bytes().to_vec()).collect();

        let entries_raw: HashMap<String, String> =
            EntryIterator::new(&state.index, &state.reader, identifiers.into_iter())?
                .map(|(k, v)| {
                    (
                        String::from_utf8_lossy(&k[..]).to_string(),
                        String::from_utf8_lossy(&v[..]).to_string(),
                    )
                })
                .collect();

        Ok(Json(entries_raw))
    }

    /// Get entries as formatted data
    ///
    /// Entries will be returned as one data stream, in the requested format.
    #[oai(path = "/entry/format/:output_format", method = "post")]
    async fn get_entry_formatted(
        &self,
        state: Data<&State>,
        output_format: Path<String>,
        data: Json<EntryRequest>,
    ) -> poem::Result<GetEntryResponse> {
        let span = span!(Level::DEBUG, "setup").entered();
        let now = std::time::Instant::now();

        let identifiers: HashSet<Vec<u8>> =
            data.ids.iter().map(|id| id.as_bytes().to_vec()).collect();

        let output_format: <T as WriteEntry>::OutputFormat = match output_format.parse() {
            Ok(of) => of,
            Err(e) => return Ok(GetEntryResponse::BadOutputFormat(PlainText(e.to_string()))),
        };

        event!(Level::DEBUG, "setup took {:?}", now.elapsed());
        span.exit();

        let span = span!(Level::DEBUG, "write_output").entered();
        let now = std::time::Instant::now();

        let mut buffer = Vec::new();
        output_format.write(
            &mut buffer,
            Box::new(EntryIterator::new(
                &state.index,
                &state.reader,
                identifiers.into_iter(),
            )?),
        )?;

        event!(Level::DEBUG, "output rendering took {:?}", now.elapsed());
        span.exit();

        Ok(GetEntryResponse::Ok(
            Binary(buffer),
            output_format.content_type().to_string(),
        ))
    }

    /// Search entries via full-text search
    #[oai(path = "/search", method = "post")]
    async fn search(
        &self,
        state: Data<&State>,
        data: Json<SearchRequest>,
    ) -> poem::Result<SearchResponse> {
        let span = span!(Level::DEBUG, "setup").entered();
        let now = std::time::Instant::now();

        let query = match state.query_parser.parse_query(&data.query) {
            Ok(q) => q,
            Err(e) => {
                return Ok(SearchResponse::BadQuery(PlainText(format!(
                    "Bad query: {}",
                    e
                ))))
            }
        };

        event!(Level::TRACE, "query parsing took {:?}", now.elapsed());
        let now = std::time::Instant::now();

        let searcher = state.reader.searcher();
        let schema = state.index.schema();

        let f_identifier = schema.get_field("identifier").unwrap();
        let f_file = schema.get_field("file").unwrap();
        let f_start = schema.get_field("start").unwrap();
        let f_length = schema.get_field("length").unwrap();

        event!(Level::TRACE, "searcher setup took {:?}", now.elapsed());
        span.exit();

        let span = span!(Level::DEBUG, "actual_search").entered();
        let now = std::time::Instant::now();

        let mut hits = Vec::new();

        let (total, docs) = searcher
            .search(
                &query,
                &(
                    tantivy::collector::Count,
                    tantivy::collector::TopDocs::with_limit(data.limit).and_offset(data.offset),
                ),
            )
            .map_err(|e| anyhow::Error::from(e))?;

        for (score, doc_address) in docs {
            let span_inner = span!(Level::DEBUG, "render_result").entered();
            let now_inner = std::time::Instant::now();

            let doc = searcher
                .doc(doc_address)
                .map_err(|e| anyhow::Error::from(e))?;
            let identifier = doc
                .get_first(f_identifier)
                .unwrap()
                .as_text()
                .unwrap()
                .to_string();

            let file = doc.get_first(f_file).unwrap().as_text().unwrap();
            let start = doc.get_first(f_start).unwrap().as_u64().unwrap();
            let length = doc.get_first(f_length).unwrap().as_u64().unwrap();

            event!(Level::TRACE, "retrieved doc in {:?}", now_inner.elapsed());
            let now_inner = std::time::Instant::now();

            let mut f_database = File::open(file).map_err(|e| anyhow::Error::from(e))?;
            let mut buffer = vec![0; length as usize];
            f_database
                .seek(SeekFrom::Start(start))
                .map_err(|e| anyhow::Error::from(e))?;
            f_database
                .read_exact(&mut buffer)
                .map_err(|e| anyhow::Error::from(e))?;

            event!(Level::TRACE, "read buffer in {:?}", now_inner.elapsed());
            let now_inner = std::time::Instant::now();

            let description =
                T::parse_brief_description(&buffer[..]).map_err(|e| anyhow::Error::from(e))?;

            event!(
                Level::TRACE,
                "parse_brief_description in {:?}",
                now_inner.elapsed()
            );

            hits.push(ScoredHit {
                score,
                identifier,
                description,
            });

            span_inner.exit();
        }

        event!(Level::DEBUG, "searching took {:?}", now.elapsed());
        span.exit();

        let res = SearchHits { hits, total };

        Ok(SearchResponse::Identifiers(Json(res)))
    }
}
