use poem::{
    async_trait,
    http::{Method, StatusCode},
    Endpoint, Request, Response,
};
use rust_embed::RustEmbed;
use serde::Serialize;

#[derive(RustEmbed)]
#[folder = "search-ui/dist/"]
struct Assets;

#[derive(Serialize)]
pub struct AssetEndpoint {
    #[serde(rename = "programName")]
    pub program_name: String,

    #[serde(rename = "version")]
    pub version: String,

    #[serde(rename = "outputFormats")]
    pub output_formats: Vec<String>,
}

#[async_trait]
impl Endpoint for AssetEndpoint {
    type Output = Response;

    async fn call(&self, req: Request) -> Result<Self::Output, poem::Error> {
        if req.method() != Method::GET {
            return Err(StatusCode::METHOD_NOT_ALLOWED.into());
        }

        // transform request uri
        let mut uri = req.uri().path().trim_matches('/');
        if uri.is_empty() {
            uri = "index.html";
        }

        if uri == "config" {
            let config = serde_json::to_vec(self).map_err(|e| anyhow::Error::from(e))?;

            return Ok(Response::builder()
                .header(poem::http::header::CONTENT_TYPE, "application/json")
                .body(config));
        }

        let asset = Assets::get(uri).ok_or(poem::Error::from_status(StatusCode::NOT_FOUND))?;

        // try to see if the file hash matches a provided etag
        let hash = hex::encode(asset.metadata.sha256_hash());
        if req
            .headers()
            .get(poem::http::header::IF_NONE_MATCH)
            .map(|etag| etag.to_str().unwrap_or("000000").eq(&hash))
            .unwrap_or(false)
        {
            return Err(StatusCode::NOT_MODIFIED.into());
        }

        // otherwise, return 200 with etag hash
        let body: Vec<u8> = asset.data.into();
        let mime = mime_guess::from_path(uri).first_or_octet_stream();
        Ok(Response::builder()
            .header(poem::http::header::CONTENT_TYPE, mime.as_ref())
            .header(poem::http::header::ETAG, hash)
            .body(body))
    }
}
