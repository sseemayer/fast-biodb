use std::str::FromStr;

use crate::format::{ParseEntry, WriteEntry};
use anyhow::Result;
use serde::Serialize;
use structopt::StructOpt;

mod get;
mod index;
mod scan;
mod utils;

mod search;

#[cfg(feature = "server")]
mod serve;

/// Utilities for quickly working with bioinformatics flatfile databases
#[derive(StructOpt)]
pub enum Cli<T>
where
    T: ParseEntry + WriteEntry + Serialize + Send + Sync + 'static,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err: std::error::Error + Send + Sync,
{
    /// Debug the parser by parsing all entries
    Scan(crate::cli::scan::Scan<T>),

    /// Create a database index from a flatfile
    Index(crate::cli::index::Index<T>),

    /// Get an entry from an indexed database
    Get(crate::cli::get::Get<T>),

    #[cfg(feature = "server")]
    /// Open a REST webserver serving data from the database using the index
    Serve(crate::cli::serve::Serve<T>),

    /// Full-text searching
    Search(crate::cli::search::Search<T>),
}

impl<T> Cli<T>
where
    T: ParseEntry + WriteEntry + Serialize + Send + Sync + 'static,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err: std::error::Error + Send + Sync,
{
    fn run(self) -> Result<()> {
        match self {
            Cli::Scan(d) => d.run(),
            Cli::Index(i) => i.run(),
            Cli::Get(g) => g.run(),

            #[cfg(feature = "server")]
            Cli::Serve(s) => s.run(),

            Cli::Search(s) => s.run(),
        }
    }

    pub fn main() -> Result<()> {
        let cli = Self::from_args();

        if let Err(e) = cli.run() {
            eprintln!("{}", e);
            std::process::exit(1);
        }

        Ok(())
    }
}
