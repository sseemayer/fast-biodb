use std::{io::Write, path::PathBuf, str::FromStr};

use anyhow::{Context, Result};
use structopt::StructOpt;

use tracing::{event, Level};

use crate::{
    cli::utils::IdentifierSource,
    format::{OutputFormat, ParseEntry, WriteEntry},
    search::EntryIterator,
};

#[derive(StructOpt)]
pub struct Get<T>
where
    T: ParseEntry + WriteEntry + 'static,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err: std::error::Error,
{
    /// Database index to search in
    #[structopt(short = "d", long)]
    database: PathBuf,

    /// Output format
    #[structopt(short = "f", long, default_value = "flat")]
    output_format: <T as WriteEntry>::OutputFormat,

    /// Streaming mode - output entries immediately. If rendering to JSON, this will mean
    /// one JSON document per line.
    #[structopt(long)]
    streaming: bool,

    #[structopt(flatten)]
    identifier_source: IdentifierSource,

    #[structopt(skip)]
    _marker: std::marker::PhantomData<T>,
}

impl<T> Get<T>
where
    T: ParseEntry + WriteEntry + Send + Sync + 'static,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err: std::error::Error,
{
    pub fn run(&self) -> Result<()> {
        let writer = std::io::stdout();
        let mut writer_lock = writer.lock();

        let identifiers = self.identifier_source.get_identifiers()?;

        if self.streaming {
            for (id, entry_buffer) in
                EntryIterator::from_identifiers(&self.database, identifiers.into_iter())?
            {
                self.output_format.write(
                    &mut writer_lock,
                    Box::new(std::iter::once((id, entry_buffer))),
                )?;

                writer_lock.write(&['\n' as u8]).context("Write newline")?;
            }
        } else {
            let now = std::time::Instant::now();
            self.output_format.write(
                &mut writer_lock,
                Box::new(EntryIterator::from_identifiers(
                    &self.database,
                    identifiers.into_iter(),
                )?),
            )?;

            event!(Level::TRACE, "Retrieved in {:?}", now.elapsed());
        }

        Ok(())
    }
}
