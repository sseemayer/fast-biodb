use std::{collections::HashSet, fs::File, io::BufRead, io::BufReader, path::PathBuf};

use structopt::StructOpt;

use anyhow::Result;

#[derive(StructOpt)]
pub struct IdentifierSource {
    /// Load identifiers from file
    #[structopt(parse(from_os_str), short, long)]
    pub identifier_file: Option<PathBuf>,

    /// List of identifiers
    #[structopt(required_unless = "identifier-file")]
    pub identifier: Vec<String>,
}

impl IdentifierSource {
    pub fn get_identifiers(&self) -> Result<HashSet<Vec<u8>>, std::io::Error> {
        let mut identifiers = self.identifier.clone();

        if let Some(ref f) = self.identifier_file {
            if f == &PathBuf::from("-") {
                read_identifiers(&mut identifiers, &mut std::io::stdin().lock())?;
            } else {
                read_identifiers(&mut identifiers, &mut BufReader::new(File::open(f)?))?;
            }
        }

        let identifiers: HashSet<Vec<u8>> =
            identifiers.iter().map(|i| i.as_bytes().to_vec()).collect();

        Ok(identifiers)
    }
}

/// Helper method to read a file line-by-line
fn read_identifiers<'a>(
    identifiers: &mut Vec<String>,
    reader: &'a mut dyn BufRead,
) -> Result<(), std::io::Error> {
    for line in reader.lines() {
        let line = line?;
        let line = line.trim();
        if line.len() > 0 {
            identifiers.push(line.to_string());
        }
    }

    Ok(())
}
