use std::{
    fs::File,
    io::{Read, Seek, Write},
    path::PathBuf,
    str::FromStr,
};

use anyhow::{Context, Result};
use structopt::StructOpt;

use crate::format::{EntryLocation, OutputFormat, ParseEntry, WriteEntry};

#[derive(StructOpt)]
pub struct Scan<T>
where
    T: ParseEntry + WriteEntry + 'static,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err: std::error::Error,
{
    /// Raw data file to parse
    database_files: Vec<PathBuf>,

    /// Output format
    #[structopt(short = "f", long, default_value = "json")]
    output_format: <T as WriteEntry>::OutputFormat,

    #[structopt(skip)]
    _marker: std::marker::PhantomData<T>,
}

impl<T> Scan<T>
where
    T: ParseEntry + WriteEntry + Send + Sync + 'static,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err: std::error::Error,
{
    pub fn run(&self) -> Result<()> {
        tracing_subscriber::fmt::init();

        let writer = std::io::stdout();
        let mut writer_lock = writer.lock();

        for dbfile in &self.database_files {
            let mut f_database_iter = File::open(dbfile).context("Open database file")?;
            let mut f_database_read = File::open(dbfile).context("Open database file")?;

            for parsed_location in T::make_location_iterator(&mut f_database_iter) {
                let EntryLocation {
                    identifiers,
                    start,
                    length,
                } = parsed_location?;

                f_database_read.seek(std::io::SeekFrom::Start(start as u64))?;

                let mut buf = vec![0; length as usize];
                f_database_read.read_exact(&mut buf)?;

                let id = identifiers[0].to_owned();
                self.output_format
                    .write(&mut writer_lock, Box::new(std::iter::once((id, buf))))?;

                writer_lock.write("\n".as_bytes())?;
            }
        }

        Ok(())
    }
}
