use std::path::PathBuf;

use anyhow::Result;
use structopt::StructOpt;

use crate::format::ParseEntry;

#[derive(StructOpt)]
pub struct Index<T>
where
    T: ParseEntry + 'static,
{
    /// Input databases
    #[structopt(parse(from_os_str))]
    input: Vec<PathBuf>,

    /// Output index folder
    #[structopt(short = "d", long, parse(from_os_str))]
    database: PathBuf,

    /// Whether to parse entries for full-text search (slower, takes more disk space)
    #[structopt(short = "-f", long)]
    full_text: bool,

    #[structopt(skip)]
    _marker: std::marker::PhantomData<T>,
}

impl<T> Index<T>
where
    T: ParseEntry,
{
    pub fn run(self) -> Result<()> {
        crate::search::create_search_index::<T>(&self.input[..], &self.database, self.full_text)?;
        Ok(())
    }
}
