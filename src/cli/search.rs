use std::{
    fs::File,
    io::{Read, Seek, SeekFrom},
    path::PathBuf,
};

use anyhow::{Context, Result};
use colored::Colorize;
use serde::Serialize;
use structopt::StructOpt;
use tantivy::collector::TopDocs;

use tracing::{event, span, Level};

use crate::format::ParseEntry;

#[derive(StructOpt)]
pub struct Search<T>
where
    T: ParseEntry + Serialize + 'static,
{
    /// Database index to search in
    #[structopt(short = "d", long)]
    database: PathBuf,

    /// The term to search for
    query: String,

    /// Maximum number of results
    #[structopt(short = "n", default_value = "10")]
    max_results: usize,

    /// JSON output mode
    #[structopt(short = "j", long)]
    json: bool,

    #[structopt(skip)]
    _marker: std::marker::PhantomData<T>,
}

#[derive(Serialize)]
struct SearchHit<T>
where
    T: ParseEntry + Serialize,
{
    score: f32,
    identifier: String,
    entry: T,
}

impl<T> Search<T>
where
    T: ParseEntry + Serialize,
{
    pub fn run(&self) -> Result<()> {
        let span_index = span!(Level::INFO, "setup").entered();
        let now = std::time::Instant::now();

        let (schema, _index, reader, query_parser) =
            crate::search::load_index(&self.database).context("Loading database index")?;

        let f_identifier = schema.get_field("identifier").unwrap();
        let f_file = schema.get_field("file").unwrap();
        let f_start = schema.get_field("start").unwrap();
        let f_length = schema.get_field("length").unwrap();

        event!(Level::INFO, "Loaded index in {:?}", now.elapsed());
        span_index.exit();

        let span_search = span!(Level::INFO, "search").entered();
        let now = std::time::Instant::now();

        let query = query_parser.parse_query(&self.query)?;

        let searcher = reader.searcher();

        if self.json {
            let hits: Vec<SearchHit<T>> = searcher
                .search(&query, &TopDocs::with_limit(self.max_results))?
                .into_iter()
                .map(|(score, doc_address)| {
                    let doc = searcher.doc(doc_address)?;
                    let identifier = doc
                        .get_first(f_identifier)
                        .unwrap()
                        .as_text()
                        .unwrap()
                        .to_string();

                    let file = doc.get_first(f_file).unwrap().as_text().unwrap();
                    let start = doc.get_first(f_start).unwrap().as_u64().unwrap();
                    let length = doc.get_first(f_length).unwrap().as_u64().unwrap();

                    let mut f_database = File::open(file)?;
                    let mut buffer = vec![0; length as usize];
                    f_database.seek(SeekFrom::Start(start))?;
                    f_database.read_exact(&mut buffer)?;

                    let entry = T::parse_entry(&buffer[..])?;
                    Ok(SearchHit {
                        score,
                        identifier,
                        entry,
                    })
                })
                .collect::<Result<Vec<SearchHit<T>>>>()?;

            let stdout = std::io::stdout();
            let writer = stdout.lock();

            serde_json::to_writer(writer, &hits)?;
        } else {
            for (score, doc_address) in
                searcher.search(&query, &TopDocs::with_limit(self.max_results))?
            {
                let doc = searcher.doc(doc_address)?;
                let identifier = doc.get_first(f_identifier).unwrap().as_text().unwrap();

                let file = doc.get_first(f_file).unwrap().as_text().unwrap();
                let start = doc.get_first(f_start).unwrap().as_u64().unwrap();
                let length = doc.get_first(f_length).unwrap().as_u64().unwrap();

                let mut f_database = File::open(file)?;
                let mut buffer = vec![0; length as usize];
                f_database.seek(SeekFrom::Start(start))?;
                f_database.read_exact(&mut buffer)?;

                let description = T::parse_brief_description(&buffer[..])?;
                println!("{:7.2} {:15} {}", score, identifier.yellow(), description);
            }
        }

        event!(Level::INFO, "Searched in {:?}", now.elapsed());

        span_search.exit();

        Ok(())
    }
}
