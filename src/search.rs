use std::{
    fs::File,
    io::{Read, Seek, SeekFrom},
    path::PathBuf,
};

use anyhow::Context;
use tantivy::{
    collector::TopDocs,
    query::QueryParser,
    schema::{Field, Schema},
    Document, Index, IndexReader,
};
use thiserror::Error;
use tracing::{event, span, Level};

use crate::{format::EntryLocation, format::ParseEntry};

#[derive(Debug, Error)]
pub enum CreateSearchIndexError<E: ParseEntry> {
    #[error("Error parsing entry location: {0}")]
    Iterate(#[source] <E as ParseEntry>::IterateErr),

    #[error("Error parsing entry: {0}")]
    Parse(#[source] <E as ParseEntry>::ParseErr),

    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    Tantivy(#[from] tantivy::TantivyError),
}

/// Create a tantivy search index for a database.
pub fn create_search_index<E: ParseEntry>(
    fns_database: &[PathBuf],
    index_path: &PathBuf,
    parse_full: bool,
) -> Result<(), CreateSearchIndexError<E>> {
    let schema = build_schema();

    std::fs::create_dir_all(index_path)?;
    let index = Index::create_in_dir(index_path, schema.clone())?;
    let mut index_writer = index.writer(50_000_000)?;

    // UNWRAP safety: the fields are always hard-coded into the schema
    let f_identifier = schema.get_field("identifier").unwrap();
    let f_file = schema.get_field("file").unwrap();
    let f_start = schema.get_field("start").unwrap();
    let f_length = schema.get_field("length").unwrap();
    let f_data = schema.get_field("data").unwrap();

    for fn_database in fns_database {
        let mut f_database_iter = File::open(fn_database)?;
        let mut f_database_read = File::open(fn_database)?;
        for parsed_location in E::make_location_iterator(&mut f_database_iter) {
            let EntryLocation {
                identifiers,
                start,
                length,
            } = parsed_location.map_err(|e| CreateSearchIndexError::Iterate(e))?;

            let mut document = Document::default();
            document.add_text(f_file, fn_database.to_string_lossy());
            document.add_u64(f_start, start as u64);
            document.add_u64(f_length, length as u64);

            for identifier in identifiers {
                document.add_text(f_identifier, String::from_utf8_lossy(&identifier[..]));
            }

            if parse_full {
                f_database_read.seek(std::io::SeekFrom::Start(start as u64))?;

                let mut buf = vec![0; length as usize];
                f_database_read.read_exact(&mut buf)?;

                let entry: E =
                    E::parse_entry(&buf).map_err(|e| CreateSearchIndexError::Parse(e))?;
                for datum in entry.full_text_data() {
                    document.add_text(f_data, datum);
                }
            }

            index_writer.add_document(document)?;
        }
    }

    index_writer.commit()?;

    Ok(())
}

#[derive(Debug, Error)]
pub enum LoadSearchIndexError {
    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    Tantivy(#[from] tantivy::TantivyError),
}

pub fn load_index(
    index_path: &PathBuf,
) -> Result<(Schema, Index, IndexReader, QueryParser), LoadSearchIndexError> {
    let span = span!(Level::INFO, "load_index").entered();
    let now = std::time::Instant::now();

    event!(Level::DEBUG, "loading index from {}", index_path.display());

    let index = Index::open_in_dir(index_path)?;
    let schema = index.schema();

    event!(Level::TRACE, "setup Index in {:?}", now.elapsed());
    let now = std::time::Instant::now();

    let reader = index.reader()?;

    event!(Level::TRACE, "setup IndexReader in {:?}", now.elapsed());
    let now = std::time::Instant::now();

    let f_identifier = schema.get_field("identifier").unwrap();
    let f_data = schema.get_field("data").unwrap();
    let query_parser = QueryParser::for_index(&index, vec![f_identifier, f_data]);

    event!(Level::TRACE, "setup QueryParser in {:?}", now.elapsed());

    span.exit();

    Ok((schema, index, reader, query_parser))
}

pub fn build_schema() -> Schema {
    use tantivy::schema::{STORED, STRING, TEXT};

    let mut schema_builder = Schema::builder();
    schema_builder.add_text_field("identifier", STORED | STRING);
    schema_builder.add_text_field("file", STORED);
    schema_builder.add_u64_field("start", STORED);
    schema_builder.add_u64_field("length", STORED);
    schema_builder.add_text_field("data", TEXT);

    schema_builder.build()
}

pub struct EntryIterator<I: Iterator<Item = Vec<u8>>> {
    identifiers: I,

    f_identifier: Field,
    f_file: Field,
    f_start: Field,
    f_length: Field,

    searcher: tantivy::LeasedItem<tantivy::Searcher>,
}

impl<I: Iterator<Item = Vec<u8>>> EntryIterator<I> {
    pub fn new(index: &Index, reader: &IndexReader, identifiers: I) -> anyhow::Result<Self> {
        let schema = index.schema();

        let f_identifier = schema.get_field("identifier").unwrap();
        let f_file = schema.get_field("file").unwrap();
        let f_start = schema.get_field("start").unwrap();
        let f_length = schema.get_field("length").unwrap();

        let searcher = reader.searcher();

        Ok(Self {
            identifiers,
            f_identifier,
            f_file,
            f_start,
            f_length,
            searcher,
        })
    }

    pub fn from_identifiers(index_file: &PathBuf, identifiers: I) -> anyhow::Result<Self> {
        let (_schema, index, reader, _query_parser) =
            crate::search::load_index(index_file).context("Loading database index")?;

        let res = Self::new(&index, &reader, identifiers);

        res
    }
}

impl<I: Iterator<Item = Vec<u8>>> Iterator for EntryIterator<I> {
    type Item = (Vec<u8>, Vec<u8>);

    fn next(&mut self) -> Option<Self::Item> {
        let identifier = self.identifiers.next()?;
        let query = tantivy::query::TermQuery::new(
            tantivy::Term::from_field_text(
                self.f_identifier,
                &String::from_utf8_lossy(&identifier[..]).to_string(),
            ),
            tantivy::schema::IndexRecordOption::Basic,
        );
        let mut docs = self
            .searcher
            .search(&query, &TopDocs::with_limit(1))
            .expect("search");

        let (_score, doc_address) = docs.pop()?;

        let doc = self.searcher.doc(doc_address).expect("Retrieve document");

        let file = doc.get_first(self.f_file).unwrap().as_text().unwrap();
        let start = doc.get_first(self.f_start).unwrap().as_u64().unwrap();
        let length = doc.get_first(self.f_length).unwrap().as_u64().unwrap();

        let mut f_database = File::open(file).expect("Open database file");
        let mut buffer = vec![0; length as usize];
        f_database
            .seek(SeekFrom::Start(start))
            .expect("Seek in database");
        f_database.read_exact(&mut buffer).expect("Read raw entry");

        Some((identifier, buffer))
    }
}
