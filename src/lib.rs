pub mod format;

pub mod cli;

pub mod search;

pub const VERSION: &'static str = env!("CARGO_PKG_VERSION");
