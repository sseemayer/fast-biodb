pub mod brenda;
pub mod fasta;
pub mod genbank;
pub mod swiss;

use anyhow::{Context, Result};

use thiserror::Error;

use std::{
    collections::HashMap,
    io::{Read, Seek, Write},
    str::FromStr,
};

/// A memory location for a database entry, together with a list of associated identifiers
pub struct EntryLocation {
    pub identifiers: Vec<Vec<u8>>,
    pub start: usize,
    pub length: usize,
}

/// An entry type that can be parsed from a bioinformatics database.
///
/// There are two parser stages - the 'fast' stage (`make_boundary_iterator`) which only detects
/// entry boundaries, returning an Iterator over `ParsedEntry` fields containing identifiers and
/// memory offsets, and a 'slow' stage (`parse_entry`) that returns fully-parsed entries from a
/// memory buffer.
pub trait ParseEntry
where
    Self: Sized + std::fmt::Debug,
{
    type ParseErr: std::error::Error + Send + Sync + 'static;
    type IterateErr: std::error::Error + Send + Sync + 'static;

    /// Slow-phase: Parse a memory buffer in `data` and return a fully parsed entry
    fn parse_entry(data: &[u8]) -> std::result::Result<Self, <Self as ParseEntry>::ParseErr>;

    /// Fast-phase: Iterate over a file-like object and return an iterator of entry boundaries and identifiers.
    fn make_location_iterator<F: Read + Seek>(
        file: &mut F,
    ) -> Box<
        dyn Iterator<Item = std::result::Result<EntryLocation, <Self as ParseEntry>::IterateErr>>
            + '_,
    >;

    /// From a parsed entry, return a list of data points to be indexed for full-text search
    fn full_text_data(&self) -> Vec<&str> {
        Vec::new()
    }

    /// Parse the raw data into a brief description for the entry
    fn parse_brief_description(
        data: &[u8],
    ) -> std::result::Result<String, <Self as ParseEntry>::ParseErr>;
}

pub trait WriteEntry
where
    Self: ParseEntry,
    <<Self as WriteEntry>::OutputFormat as FromStr>::Err: std::error::Error,
{
    type OutputFormat: OutputFormat<Self>;
}

pub trait OutputFormat<T>: Send + Sync + std::fmt::Debug + FromStr + 'static
where
    T: ParseEntry + WriteEntry,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err: std::error::Error,
{
    /// Write formatted entries for identifiers identifiers_ref retrieved from entry_source to a writer
    fn write(
        &self,
        writer: &mut dyn Write,
        entries: Box<dyn Iterator<Item = (Vec<u8>, Vec<u8>)>>,
    ) -> Result<()>;

    /// Content type to send out as part of API responses
    fn content_type(&self) -> &'static str;

    /// List of valid formats
    fn valid_formats() -> &'static [&'static str];
}

#[derive(Debug)]
pub enum GenericOutputFormat {
    Flat,
    Json,
}

impl<T> OutputFormat<T> for GenericOutputFormat
where
    T: ParseEntry + WriteEntry + serde::Serialize,
    <<T as WriteEntry>::OutputFormat as FromStr>::Err: std::error::Error,
{
    fn write(
        &self,
        writer: &mut dyn Write,
        entries: Box<dyn Iterator<Item = (Vec<u8>, Vec<u8>)>>,
    ) -> Result<()> {
        match self {
            GenericOutputFormat::Flat => {
                for (_id, entry) in entries {
                    writer.write_all(&entry).context("writing flat entry")?;
                }
            }
            GenericOutputFormat::Json => {
                let entries_parsed = entries
                    .map(|(id, entry)| {
                        Ok((
                            String::from_utf8_lossy(&id[..]).to_string(),
                            T::parse_entry(&entry[..]).map_err(|e| anyhow::Error::from(e))?,
                        ))
                    })
                    .collect::<Result<HashMap<String, T>>>()?;

                serde_json::ser::to_writer(writer, &entries_parsed).context("write JSON entry")?;
            }
        }
        Ok(())
    }

    fn content_type(&self) -> &'static str {
        match self {
            GenericOutputFormat::Flat => "text/plain",
            GenericOutputFormat::Json => "application/json",
        }
    }

    fn valid_formats() -> &'static [&'static str] {
        &["flat", "json"]
    }
}

#[derive(Error, Debug)]
#[error("Bad output format {value} - supported are {supported}")]
pub struct OutputFormatError {
    value: String,
    supported: String,
}

impl std::str::FromStr for GenericOutputFormat {
    type Err = OutputFormatError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match &s.to_lowercase()[..] {
            "flat" => Ok(GenericOutputFormat::Flat),
            "json" => Ok(GenericOutputFormat::Json),

            _ => Err(OutputFormatError {
                value: s.to_string(),
                supported: "flat, json".to_string(),
            }),
        }
    }
}
