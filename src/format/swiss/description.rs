use std::str::FromStr;

use super::{evidence::Evidenced, ParserState, RootParser, Subparser, SwissEntry, SwissParseError};

#[derive(Debug, Default, PartialEq, Eq, serde::Serialize)]
pub struct EntryName {
    /// Entry name from the "DE   RecName:" fields
    pub rec_name: Option<EntryNameCategory>,

    /// Alternative entry names from the "DE   AltName:" fields
    pub alt_names: Vec<EntryNameCategory>,

    /// Submitter-provided names from the "DE   SubName:" fields
    pub sub_names: Vec<EntryNameCategory>,
}

impl EntryName {
    /// Get a human-readable name for this entry.
    ///
    /// Prefers RecName over AltName over SubName.
    /// Prefers Long names over Short names.
    pub fn get_preferred_name(&self) -> Option<String> {
        self.rec_name
            .as_ref()
            .or(self.alt_names.first())
            .or(self.sub_names.first())
            .and_then(|n| n.full.as_ref().or(n.short.first()).map(|s| s.as_str()))
    }

    fn assign(
        &mut self,
        category: &DescriptionCategory,
        subcategory: &str,
        content: &str,
        append: bool,
    ) -> Result<(), SwissParseError> {
        let mut target = if append {
            match category {
                DescriptionCategory::RecName => self.rec_name.take().unwrap_or_default(),
                DescriptionCategory::AltName => self.alt_names.pop().unwrap_or_default(),
                DescriptionCategory::SubName => self.sub_names.pop().unwrap_or_default(),
            }
        } else {
            Default::default()
        };

        target.assign(subcategory, content)?;

        match category {
            DescriptionCategory::RecName => self.rec_name = Some(target),
            DescriptionCategory::AltName => self.alt_names.push(target),
            DescriptionCategory::SubName => self.sub_names.push(target),
        }

        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Eq, serde::Serialize)]
pub struct EntryNameCategory {
    pub full: Option<Evidenced<String>>,
    pub short: Vec<Evidenced<String>>,
    pub ecs: Vec<Evidenced<String>>,
    pub allergen: Vec<Evidenced<String>>,
    pub biotech: Vec<Evidenced<String>>,
    pub cd_antigens: Vec<Evidenced<String>>,
    pub inns: Vec<Evidenced<String>>,
}

impl EntryNameCategory {
    fn assign(&mut self, subcategory: &str, content: &str) -> Result<(), SwissParseError> {
        Ok(match subcategory {
            "Full" => self.full = Some(content.parse()?),
            "Short" => self.short.push(content.parse()?),
            "EC" => self.ecs.push(content.parse()?),
            "Allergen" => self.allergen.push(content.parse()?),
            "Biotech" => self.biotech.push(content.parse()?),
            "CD_antigen" => self.cd_antigens.push(content.parse()?),
            "INN" => self.inns.push(content.parse()?),
            _ => {
                return Err(SwissParseError::BadState {
                    context: "Assigning EntryNameCategory",
                    data: subcategory.to_string(),
                    reason: "Unknown Subcategory",
                });
            }
        })
    }
}

#[derive(PartialEq, Eq, Debug)]
pub(crate) enum DescriptionCategory {
    RecName,
    AltName,
    SubName,
}

impl FromStr for DescriptionCategory {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "RecName" => DescriptionCategory::RecName,
            "AltName" => DescriptionCategory::AltName,
            "SubName" => DescriptionCategory::SubName,

            _ => {
                return Err(SwissParseError::BadState {
                    context: "Parsing description category",
                    data: s.to_string(),
                    reason: "Unknown category",
                })
            }
        })
    }
}

/// Internal state of parsing a DE block
#[derive(PartialEq, Eq, Debug)]
pub(crate) enum DescriptionParser {
    Root {
        category: Option<DescriptionCategory>,
    },

    Contains {
        category: Option<DescriptionCategory>,
    },

    Includes {
        category: Option<DescriptionCategory>,
    },
}

impl DescriptionParser {
    pub(crate) fn assign(
        &self,
        subcategory: &str,
        content: &str,
        target: &mut SwissEntry,
        append: bool,
    ) -> Result<(), SwissParseError> {
        match self {
            DescriptionParser::Contains { category } => {
                let mut contained = target.contains.pop().unwrap_or_default();
                let category = category.as_ref().ok_or_else(|| SwissParseError::BadState {
                    context: "Parsing DE line (contained)",
                    data: String::new(),
                    reason: "Expected there to be a contained category to assign to",
                })?;
                contained.assign(&category, subcategory, content, append)?;
                target.contains.push(contained);
            }
            DescriptionParser::Includes { category } => {
                let mut included = target.includes.pop().unwrap_or_default();
                let category = category.as_ref().ok_or_else(|| SwissParseError::BadState {
                    context: "Parsing DE line (included)",
                    data: String::new(),
                    reason: "Expected there to be a included category to assign to",
                })?;
                included.assign(&category, subcategory, content, append)?;
                target.includes.push(included);
            }
            DescriptionParser::Root { category } => {
                let category = category.as_ref().ok_or_else(|| SwissParseError::BadState {
                    context: "Parsing DE line (root)",
                    data: String::new(),
                    reason: "Expected there to be a root category to assign to",
                })?;
                target
                    .name
                    .assign(&category, subcategory, content, append)?;
            }
        }

        Ok(())
    }
}

#[derive(PartialEq, Eq)]
enum NestingMode {
    Root,
    Contained,
    Included,
}

impl Subparser for DescriptionParser {
    fn enter(
        tag: &str,
        data: &str,
        target: &mut super::SwissEntry,
    ) -> Result<ParserState, SwissParseError>
    where
        Self: Sized,
    {
        DescriptionParser::Root { category: None }.input(tag, data, target)
    }

    fn input(
        self,
        tag: &str,
        data: &str,
        mut target: &mut SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        if tag != "DE" {
            return Ok(RootParser::enter(tag, data, &mut target)?);
        }

        let nested = match (&self, &data[..2] == "  ") {
            (DescriptionParser::Contains { .. }, true) => NestingMode::Contained,
            (DescriptionParser::Includes { .. }, true) => NestingMode::Included,
            _ => NestingMode::Root,
        };

        let data = if nested != NestingMode::Root {
            &data[2..]
        } else {
            data
        };

        let colon_pos = data
            .find(":")
            .and_then(|cp| if cp < 10 { Some(cp) } else { None });

        let category = colon_pos.map(|cp| data[..cp].trim()).unwrap_or("");

        if category == "Flags" {
            let flags = data[7..data.len() - 1].trim().to_string();
            target.flags = Some(flags);
            return Ok(ParserState::Description(DescriptionParser::Root {
                category: None,
            }));
        } else if category == "Contains" {
            target.contains.push(Default::default());
            return Ok(ParserState::Description(DescriptionParser::Contains {
                category: None,
            }));
        } else if category == "Includes" {
            target.includes.push(Default::default());
            return Ok(ParserState::Description(DescriptionParser::Includes {
                category: None,
            }));
        }

        let equal_pos = data.find("=").ok_or_else(|| SwissParseError::BadState {
            context: "Parsing DE line subcategory",
            data: data.to_string(),
            reason: "Expected line to be splittable with '='",
        })?;
        let subcategory = data[colon_pos.unwrap_or(7) + 1..equal_pos].trim();
        let content = data[equal_pos + 1..data.len() - 1].trim();

        if category.len() == 0 {
            // keep adding to the same category
            self.assign(subcategory, content, &mut target, true)?;
            Ok(ParserState::Description(self))
        } else {
            // a new category was specified
            let category: Option<DescriptionCategory> = Some(category.parse()?);

            let new_mode = match nested {
                NestingMode::Contained => DescriptionParser::Contains { category },
                NestingMode::Included => DescriptionParser::Includes { category },
                NestingMode::Root => DescriptionParser::Root { category },
            };

            new_mode.assign(subcategory, content, &mut target, false)?;

            Ok(ParserState::Description(new_mode))
        }
    }
}
