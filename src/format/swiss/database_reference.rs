use std::str::FromStr;

use super::{ParserState, RootParser, Subparser, SwissParseError};

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "type")]
pub enum DatabaseReference {
    /// EMBL nucleotide sequences
    Embl {
        accession_number: String,
        protein_id: String,

        #[serde(flatten)]
        status_identifier: EmblStatusIdentifier,

        #[serde(flatten)]
        molecule_type: EmblMoleculeType,
    },

    /// BRENDA enzyme informations
    Brenda {
        ec_number: String,
        organism_code: usize,
    },

    /// CAZy carbohydrage-active enzymes
    Cazy { family: String, family_name: String },

    /// ChEMBL bioactive drug-like small molecules
    ChEmbl { identifier: String },

    /// eggNOG non-supervised orthologous groups
    EggNog {
        group_name: String,
        taxonomic_scope: String,
    },

    /// GO Gene Ontology
    Go {
        /// A GO identifier
        go_term: String,

        /// The part of the Gene Ontology this term refers to
        #[serde(flatten)]
        ontology: GoOntology,

        /// Human-readable, possibly abbreviated name of the GO term
        term_data: String,

        #[serde(flatten)]
        evidence_code: GoEvidenceCode,

        /// Details for the evidence given
        evidence_details: Option<String>,
    },

    /// InterPro protein domains and functional sites
    InterPro {
        identifier: String,
        entry_name: String,
    },

    /// KEGG kyoto encyclopedia of genes and genomes
    Kegg { identifier: String },

    /// PDB protein structures
    Pdb {
        entry_name: String,

        #[serde(flatten)]
        method: PdbMethod,
        resolution: String,
        chain_coverage: Vec<String>,
    },

    /// PDBsum
    PdbSum { entry_name: String },

    /// Pfam protein domains and families
    Pfam {
        identifier: String,
        entry_name: String,
        n_hits: usize,
    },

    /// PIR Protein information resource
    Pir {
        accession_number: String,
        entry_name: String,
    },

    /// PROSITE protein domains and families
    Prosite {
        accession_number: String,
        entry_name: String,
        n_hits: usize,
    },

    /// RefSeq NCBI reference sequences
    RefSeq {
        accession_number: String,
        sequence_identifier: String,
        isoform: Option<String>,
    },

    /// A reference for which no parser exists
    NotParsed {
        #[serde(rename = "reference_name")]
        resource_abbreviation: String,
        resource_identifier: String,
        optional_information_1: Option<String>,
        optional_information_2: Option<String>,
        optional_information_3: Option<String>,
    },
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "method")]
pub enum PdbMethod {
    XRayCrystallography,
    NmrSpectroscopy,
    ElectronMicroscopy,
    FiberDiffraction,
    InfraredSpectroscopy,
    PredictedModel,
    NeutronDiffraction,
    NotParsed {
        #[serde(rename = "method_details")]
        content: String,
    },
}

impl FromStr for PdbMethod {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "X-ray" => PdbMethod::XRayCrystallography,
            "NMR" => PdbMethod::NmrSpectroscopy,
            "EM" => PdbMethod::ElectronMicroscopy,
            "Fiber" => PdbMethod::FiberDiffraction,
            "IR" => PdbMethod::InfraredSpectroscopy,
            "Model" => PdbMethod::PredictedModel,
            "Neutron" => PdbMethod::NeutronDiffraction,
            _ => PdbMethod::NotParsed {
                content: s.to_string(),
            },
        })
    }
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "ontology")]
pub enum GoOntology {
    /// P: Biological Process
    BiologicalProcess,

    /// F: Molecular Function
    MolecularFunction,

    /// C: Cellular Component
    CellularComponent,

    /// A Go Ontology for which no parser is currently implemented
    NotParsed {
        #[serde(rename = "ontology_details")]
        content: String,
    },
}

impl FromStr for GoOntology {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "P" => GoOntology::BiologicalProcess,
            "F" => GoOntology::MolecularFunction,
            "C" => GoOntology::CellularComponent,
            _ => GoOntology::NotParsed {
                content: s.to_string(),
            },
        })
    }
}

/// Evidence for GO term assignment (http://geneontology.org/docs/guide-go-evidence-codes/)
#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "evidence_code")]
pub enum GoEvidenceCode {
    // Experimental evidence codes
    InferredFromExperiment,                       // EXP
    InferredFromDirectAssay,                      // IDA
    InferredFromPhysicalInteraction,              // IPI
    InferredFromMutantPhenotype,                  // IMP
    InferredFromGeneticInteraction,               // IGI
    InferredFromExpressionPattern,                // IEP
    InferredFromHighThroughputExperiment,         // HTP
    InferredFromHighThroughputDirectAssay,        // HDA
    InferredFromHighThroughputMutantPhenotype,    // HMP
    InferredFromHighThroughputGeneticInteraction, // HGI
    InferredFromHighThroughputExpressionPattern,  // HEP

    // Phylogenetically-inferred evidence codes
    InferredFromBiologicalAspectOfAncestor,   // IBA
    InferredFromBiologicalAspectOfDescendant, // IBD
    InferredFromKeyResidues,                  // IKR
    InferredFromRapidDivergence,              // IRD

    // Computational analysis evidence codes
    InferredBySequenceOrStructureSimilarity,   // ISS
    InferredBySequenceOrthology,               // ISO
    InferredBySequenceAlignment,               // ISA
    InferredBySequenceModel,                   // ISM
    InferredFromGenomicContext,                // IGC
    InferredFromReviewedComputationalAnalysis, // RCA

    // Author statement evidence codes
    TraceableAuthorStatement,    // TAS
    NonTraceableAuthorStatement, // NAS

    // Curator statement evidence codes
    InferredByCurator,         // IC
    NoBiologicalDataAvailable, // ND

    // Electronic annotation evidence codes
    InferredFromElectronicAnnotation, // IEA

    NotParsed {
        #[serde(rename = "evidence_code_details")]
        content: String,
    },
}

impl FromStr for GoEvidenceCode {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            // Experimental evidence codes
            "EXP" => GoEvidenceCode::InferredFromExperiment,
            "IDA" => GoEvidenceCode::InferredFromDirectAssay,
            "IPI" => GoEvidenceCode::InferredFromPhysicalInteraction,
            "IMP" => GoEvidenceCode::InferredFromMutantPhenotype,
            "IGI" => GoEvidenceCode::InferredFromGeneticInteraction,
            "IEP" => GoEvidenceCode::InferredFromExpressionPattern,
            "HTP" => GoEvidenceCode::InferredFromHighThroughputExperiment,
            "HDA" => GoEvidenceCode::InferredFromHighThroughputDirectAssay,
            "HMP" => GoEvidenceCode::InferredFromHighThroughputMutantPhenotype,
            "HGI" => GoEvidenceCode::InferredFromHighThroughputGeneticInteraction,
            "HEP" => GoEvidenceCode::InferredFromHighThroughputExpressionPattern,

            // Phylogenetically-inferred evidence codes
            "IBA" => GoEvidenceCode::InferredFromBiologicalAspectOfAncestor,
            "IBD" => GoEvidenceCode::InferredFromBiologicalAspectOfDescendant,
            "IKR" => GoEvidenceCode::InferredFromKeyResidues,
            "IRD" => GoEvidenceCode::InferredFromRapidDivergence,

            // Computational analysis evidence codes
            "ISS" => GoEvidenceCode::InferredBySequenceOrStructureSimilarity,
            "ISO" => GoEvidenceCode::InferredBySequenceOrthology,
            "ISA" => GoEvidenceCode::InferredBySequenceAlignment,
            "ISM" => GoEvidenceCode::InferredBySequenceModel,
            "IGC" => GoEvidenceCode::InferredFromGenomicContext,
            "RCA" => GoEvidenceCode::InferredFromReviewedComputationalAnalysis,

            // Author statement evidence codes
            "TAS" => GoEvidenceCode::TraceableAuthorStatement,
            "NAS" => GoEvidenceCode::NonTraceableAuthorStatement,

            // Curator statement evidence codes
            "IC" => GoEvidenceCode::InferredByCurator,
            "ND" => GoEvidenceCode::NoBiologicalDataAvailable,

            // Electronic annotation evidence codes
            "IEA" => GoEvidenceCode::InferredFromElectronicAnnotation,

            _ => GoEvidenceCode::NotParsed {
                content: s.to_string(),
            },
        })
    }
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "status")]
pub enum EmblStatusIdentifier {
    AllOk,
    AltInit,
    AltTerm,
    AltFrame,
    AltSeq,
    Joined,
    NotAnnotatedCds,
    NotParsed {
        #[serde(rename = "status_details")]
        content: String,
    },
}

impl FromStr for EmblStatusIdentifier {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "-" => EmblStatusIdentifier::AllOk,
            "ALT_INIT" => EmblStatusIdentifier::AltInit,
            "ALT_TERM" => EmblStatusIdentifier::AltTerm,
            "ALT_FRAME" => EmblStatusIdentifier::AltFrame,
            "ALT_SEQ" => EmblStatusIdentifier::AltSeq,
            "JOINED" => EmblStatusIdentifier::Joined,
            "NOT_ANNOTATED_CDS" => EmblStatusIdentifier::NotAnnotatedCds,
            _ => EmblStatusIdentifier::NotParsed {
                content: s.to_string(),
            },
        })
    }
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "molecule_type")]
pub enum EmblMoleculeType {
    GenomicDna,
    GenomicRna,
    TranscribedRna,
    #[serde(rename = "mRNA")]
    MRna,
    ViralCDna,
    UnassignedDna,
    UnassignedRna,
    OtherDna,
    OtherRna,
    Unknown,

    NotParsed {
        #[serde(rename = "molecule_type_details")]
        content: String,
    },
}

impl FromStr for EmblMoleculeType {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "Genomic_DNA" => EmblMoleculeType::GenomicDna,
            "Genomic_RNA" => EmblMoleculeType::GenomicRna,
            "Transcribed_RNA" => EmblMoleculeType::TranscribedRna,
            "mRNA" => EmblMoleculeType::MRna,
            "Viral_cDNA" => EmblMoleculeType::ViralCDna,
            "Unassigned_DNA" => EmblMoleculeType::UnassignedDna,
            "Unassigned_RNA" => EmblMoleculeType::UnassignedRna,
            "Other_DNA" => EmblMoleculeType::OtherDna,
            "Other_RNA" => EmblMoleculeType::OtherRna,
            "-" => EmblMoleculeType::Unknown,

            _ => EmblMoleculeType::NotParsed {
                content: s.to_string(),
            },
        })
    }
}

pub(crate) struct DatabaseReferenceParser;

impl Subparser for DatabaseReferenceParser {
    fn enter(
        tag: &str,
        data: &str,
        target: &mut super::SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        if tag != "DR" {
            return RootParser::enter(tag, data, target);
        }

        let tokens: Vec<&str> = data.split("; ").collect();
        let resource_abbreviation = tokens[0];
        let id = tokens[1].trim_end_matches(".").to_string();
        let opt1: Result<String, SwissParseError> = tokens
            .get(2)
            .map(|o| o.trim_end_matches(".").to_string())
            .ok_or_else(|| SwissParseError::BadState {
                context: "parsing database reference",
                data: data.to_string(),
                reason: "Getting a missing optional token 1",
            });
        let opt2: Result<String, SwissParseError> = tokens
            .get(3)
            .map(|o| o.trim_end_matches(".").to_string())
            .ok_or_else(|| SwissParseError::BadState {
                context: "parsing database reference",
                data: data.to_string(),
                reason: "Getting a missing optional token 2",
            });
        let opt3: Result<String, SwissParseError> = tokens
            .get(4)
            .map(|o| o.trim_end_matches(".").to_string())
            .ok_or_else(|| SwissParseError::BadState {
                context: "parsing database reference",
                data: data.to_string(),
                reason: "Getting a missing optional token 3",
            });

        let reference = match resource_abbreviation {
            "EMBL" => DatabaseReference::Embl {
                accession_number: id,
                protein_id: opt1?,
                status_identifier: opt2?.parse()?,
                molecule_type: opt3?.parse()?,
            },

            "BRENDA" => {
                let opt1 = opt1?;
                DatabaseReference::Brenda {
                    ec_number: id,
                    organism_code: opt1.parse()?,
                }
            }

            "CAZy" => DatabaseReference::Cazy {
                family: id,
                family_name: opt1?,
            },

            "ChEMBL" => DatabaseReference::ChEmbl { identifier: id },

            "eggNOG" => DatabaseReference::EggNog {
                group_name: id,
                taxonomic_scope: opt1?,
            },

            "GO" => {
                let opt1 = opt1?;
                let ontology = opt1[..1].parse()?;
                let term_data = opt1[2..].to_string();

                let opt2: String = opt2?;
                let (evidence_code, evidence_details) = if let Some(cp) = opt2.find(":") {
                    let ec = &opt2[..cp];
                    let ed = &opt2[cp + 1..];
                    (ec.parse()?, Some(ed.to_string()))
                } else {
                    (opt2.parse()?, None)
                };

                DatabaseReference::Go {
                    go_term: id,
                    ontology,
                    term_data,
                    evidence_code,
                    evidence_details,
                }
            }

            "InterPro" => DatabaseReference::InterPro {
                identifier: id,
                entry_name: opt1?,
            },

            "KEGG" => DatabaseReference::Kegg { identifier: id },

            "PDB" => DatabaseReference::Pdb {
                entry_name: id,
                method: opt1?.parse()?,
                resolution: opt2?,
                chain_coverage: opt3
                    .unwrap_or_default()
                    .split(", ")
                    .map(|s| s.to_string())
                    .collect(),
            },

            "PDBsum" => DatabaseReference::PdbSum { entry_name: id },

            "Pfam" => {
                let opt2 = opt2?;

                DatabaseReference::Pfam {
                    identifier: id,
                    entry_name: opt1?,
                    n_hits: opt2.parse()?,
                }
            }

            "PIR" => DatabaseReference::Pir {
                accession_number: id,
                entry_name: opt1?,
            },

            "PROSITE" => {
                let opt2 = opt2?;
                DatabaseReference::Prosite {
                    accession_number: id,
                    entry_name: opt1?,
                    n_hits: opt2.parse()?,
                }
            }

            "RefSeq" => DatabaseReference::RefSeq {
                accession_number: id,
                sequence_identifier: opt1?,
                isoform: opt2.ok(),
            },

            _ => DatabaseReference::NotParsed {
                resource_abbreviation: resource_abbreviation.to_string(),
                resource_identifier: id,
                optional_information_1: opt1.ok(),
                optional_information_2: opt2.ok(),
                optional_information_3: opt3.ok(),
            },
        };

        target.database_references.push(reference);

        Ok(ParserState::DatabaseReference)
    }

    fn input(
        self,
        tag: &str,
        data: &str,
        target: &mut super::SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        DatabaseReferenceParser::enter(tag, data, target)
    }
}
