use std::str::FromStr;

use super::{evidence::Evidenced, ParserState, RootParser, Subparser, SwissEntry, SwissParseError};

#[derive(Debug, Default, PartialEq, Eq, serde::Serialize)]
pub struct LiteratureReference {
    /// Number of the reference from the "RN" field
    pub number: Evidenced<usize>,

    /// Position from "RP fields
    pub position: String,

    /// Reference cross-references from "RX" fields
    pub cross_references: Vec<LiteratureReferenceType>,

    /// Title from "RT" fields
    pub title: Option<String>,

    /// Reference group from "RG" fields
    pub group: Option<String>,

    /// Authors from "RA" fields
    pub authors: Vec<String>,

    /// Location (journal) from "RL" fields
    pub location: String,

    /// Comments from "RC" fields
    pub comments: Vec<LiteratureComment>,
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "type")]
pub enum LiteratureReferenceType {
    Medline { identifier: usize },
    PubMed { identifier: usize },
    Doi { identifier: String },
    Agricola { identifier: String },

    NotParsed { identifier: String },
}

impl FromStr for LiteratureReferenceType {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (k, v) = s.split_once("=").ok_or_else(|| SwissParseError::BadState {
            context: "Parsing literature reference type",
            data: s.to_string(),
            reason: "Expected tag and ID to be splittable using '='",
        })?;

        Ok(match k {
            "MEDLINE" => LiteratureReferenceType::Medline {
                identifier: v.parse()?,
            },
            "PubMed" => LiteratureReferenceType::PubMed {
                identifier: v.parse()?,
            },
            "DOI" => LiteratureReferenceType::Doi {
                identifier: v.to_string(),
            },
            "AGRICOLA" => LiteratureReferenceType::Agricola {
                identifier: v.to_string(),
            },

            _ => LiteratureReferenceType::NotParsed {
                identifier: v.to_string(),
            },
        })
    }
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "type")]
pub enum LiteratureComment {
    Strain { content: String },
    Plasmid { content: String },
    Transposon { content: String },
    Tissue { content: String },
    NotParsed { content: String },
}

impl FromStr for LiteratureComment {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (k, v) = s.split_once("=").ok_or_else(|| SwissParseError::BadState {
            context: "Parsing literature comment",
            data: s.to_string(),
            reason: "Expected comment to be splittable with '='",
        })?;
        let content = v.to_string();

        Ok(match k {
            "STRAIN" => LiteratureComment::Strain { content },
            "PLASMID" => LiteratureComment::Plasmid { content },
            "TRANSPOSON" => LiteratureComment::Transposon { content },
            "TISSUE" => LiteratureComment::Tissue { content },
            _ => LiteratureComment::NotParsed { content },
        })
    }
}

pub(crate) struct LiteratureParser {
    current_tag: Option<(String, String)>,
}

impl LiteratureParser {
    fn finalize_tag(
        &mut self,
        tag: String,
        data: String,
        target: &mut SwissEntry,
    ) -> Result<(), SwissParseError> {
        let reference =
            target
                .literature_references
                .last_mut()
                .ok_or_else(|| SwissParseError::BadState {
                    context: "Continuing reference",
                    data: format!("{}   {}", tag, data),
                    reason: "Expected a reference to have been opened with RN tag",
                })?;

        match &tag[..] {
            "RP" => {
                reference.position = data;
            }
            "RC" => {
                let tokens = data
                    .trim_end_matches(";")
                    .split("; ")
                    .map(|t| t.parse())
                    .collect::<Result<Vec<_>, SwissParseError>>()?;

                reference.comments.extend(tokens);
            }
            "RX" => {
                let tokens = data
                    .trim_end_matches(";")
                    .split("; ")
                    .map(|t| t.parse())
                    .collect::<Result<Vec<_>, SwissParseError>>()?;

                reference.cross_references.extend(tokens);
            }
            "RG" => {
                reference.group = Some(data);
            }
            "RA" => {
                let authors = &mut reference.authors;
                authors.extend(data.trim_end_matches(";").split(", ").map(String::from));
            }
            "RT" => {
                reference.title = Some(data.trim_end_matches(';').trim_matches('"').to_string());
            }
            "RL" => reference.location = data,
            _ => {
                return Err(SwissParseError::BadState {
                    context: "Finalizing reference tag",
                    data: format!("{}   {}", tag, data),
                    reason: "Cannot finalize that tag type",
                });
            }
        }

        Ok(())
    }
}

impl Subparser for LiteratureParser {
    fn input(
        mut self,
        tag: &str,
        data: &str,
        target: &mut SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        if tag != "RN"
            && tag != "RP"
            && tag != "RC"
            && tag != "RX"
            && tag != "RG"
            && tag != "RA"
            && tag != "RT"
            && tag != "RL"
        {
            let (ct, cd) = self
                .current_tag
                .take()
                .expect("Finalize and transition away from literature");

            self.finalize_tag(ct, cd, target)?;
            return RootParser::enter(tag, data, target);
        }

        if tag == "RN" {
            // start a new reference

            let open_bracket = data.find("[").ok_or_else(|| SwissParseError::BadState {
                context: "Parsing literature RN line",
                data: data.to_string(),
                reason: "Expected opening bracket '['",
            })?;

            let close_bracket =
                data[open_bracket..]
                    .find("]")
                    .ok_or_else(|| SwissParseError::BadState {
                        context: "Parsing literature RN line",
                        data: data.to_string(),
                        reason: "Expected closing bracket ']'",
                    })?;

            let number = format!(
                "{}{}",
                &data[open_bracket + 1..open_bracket + close_bracket],
                &data[open_bracket + close_bracket + 1..]
            );

            let number = number.parse()?;

            let reference = LiteratureReference {
                number,
                ..Default::default()
            };

            target.literature_references.push(reference);

            return Ok(ParserState::Literature(self));
        }

        self.current_tag = if let Some((ct, mut cd)) = self.current_tag.take() {
            if ct != tag {
                self.finalize_tag(ct, cd, target)?;
                Some((tag.to_string(), data.to_string()))
            } else {
                if !cd.ends_with(' ') {
                    cd.push(' ');
                }
                cd.extend(data.chars());
                Some((ct, cd))
            }
        } else {
            Some((tag.to_string(), data.to_string()))
        };

        Ok(ParserState::Literature(self))
    }

    fn enter(
        tag: &str,
        data: &str,
        target: &mut SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        LiteratureParser { current_tag: None }.input(tag, data, target)
    }
}
