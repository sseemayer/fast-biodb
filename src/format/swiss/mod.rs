pub mod database_reference;
pub mod description;
pub mod evidence;
pub mod feature;
pub mod literature_reference;
mod sequence;

use anyhow::Context;
use database_reference::{DatabaseReference, DatabaseReferenceParser};
use description::{DescriptionParser, EntryName};
use evidence::Evidenced;
use feature::{Feature, FeatureParser};
use literature_reference::{LiteratureParser, LiteratureReference};
use sequence::SequenceParser;

use crate::format::{EntryLocation, ParseEntry};
use std::io::{BufRead, BufReader, Read, Seek};

use thiserror::Error;

use chrono::NaiveDate;

use super::{OutputFormat, OutputFormatError, WriteEntry};

#[derive(Debug, Default, PartialEq, Eq, serde::Serialize)]
pub struct SwissEntry {
    /// Entry identifier from the "ID" field
    pub identifier: String,

    /// Accession numbers from the "AC" field
    pub accessions: Vec<String>,

    /// Entry name from the "DE" fields
    pub name: EntryName,

    /// Contained entry names from "DE   Contains:" fields
    pub contains: Vec<EntryName>,

    /// Included domain names from "DE   Includes:" fields
    pub includes: Vec<EntryName>,

    /// Flags from "DE   Flags:" fields
    pub flags: Option<String>,

    /// Features from "FT" fields,
    pub features: Vec<Feature>,

    /// Entry sequence from the "SQ" field
    pub sequence: String,

    /// Revision dates from the "DT" field
    pub dates: Vec<EntryDate>,

    /// Source organism from the "OS" field
    pub source_organism: Option<String>,

    /// Source organism TaxID from the "OX   NCBI_TaxID=" field
    pub source_taxid: Option<Evidenced<usize>>,

    /// Literature references from the "R*" fields
    pub literature_references: Vec<LiteratureReference>,

    /// Database cross-references from the "DR" fields
    pub database_references: Vec<DatabaseReference>,
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
pub struct EntryDate {
    /// The date of the update
    date: NaiveDate,

    /// The remark associated with the update
    remark: String,
}

#[derive(Error, Debug)]
pub enum SwissParseError {
    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error("Malformatted SwissProt entry ({context}): '{data}': {reason}")]
    BadState {
        context: &'static str,
        data: String,
        reason: &'static str,
    },

    #[error(transparent)]
    DateFormat(#[from] chrono::ParseError),

    #[error(transparent)]
    IntFormat(#[from] std::num::ParseIntError),

    #[error(transparent)]
    Boxed(Box<dyn std::error::Error + Send + Sync>),
}

pub(crate) trait Subparser {
    /// Start processing the first line with this new subparser
    fn enter(
        tag: &str,
        data: &str,
        target: &mut SwissEntry,
    ) -> Result<ParserState, SwissParseError>;

    /// Continue processing additional lines with that subparser, or transition into a new
    /// subparser
    fn input(
        self,
        tag: &str,
        data: &str,
        target: &mut SwissEntry,
    ) -> Result<ParserState, SwissParseError>;
}

pub(crate) struct RootParser;

impl Subparser for RootParser {
    fn enter(
        tag: &str,
        data: &str,
        mut target: &mut SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        match tag {
            "ID" => {
                // UNWRAP safety: there has to be always at least one token
                target.identifier = data.split(" ").next().unwrap().to_string();
                Ok(ParserState::Root)
            }
            "DE" => DescriptionParser::enter(tag, data, &mut target),
            "AC" => {
                target.accessions.extend(
                    data.replace(";", "")
                        .split_whitespace()
                        .map(|s| s.to_string()),
                );
                Ok(ParserState::Root)
            }
            "OS" => {
                let mut so = target.source_organism.to_owned().unwrap_or(String::new());
                so.extend(data.trim().chars());
                target.source_organism.replace(so);

                Ok(ParserState::Root)
            }
            "OX" => {
                if data.starts_with("NCBI_TaxID=") {
                    let data = &data[11..data.len() - 1];
                    let taxid: Evidenced<usize> = data.parse()?;

                    target.source_taxid = Some(taxid);
                }
                Ok(ParserState::Root)
            }
            "DT" => {
                let (date, remark) = data[..data.len() - 1].split_once(", ").ok_or_else(|| {
                    SwissParseError::BadState {
                        context: "Parsing DT line",
                        data: data.to_string(),
                        reason: "Could not split date and remark using ', '",
                    }
                })?;

                let date = NaiveDate::parse_from_str(date, "%d-%b-%Y")?;

                let remark = remark.to_string();

                target.dates.push(EntryDate { date, remark });
                Ok(ParserState::Root)
            }
            "FT" => FeatureParser::enter(tag, data, &mut target),
            "RN" => LiteratureParser::enter(tag, data, &mut target),
            "DR" => DatabaseReferenceParser::enter(tag, data, &mut target),
            "SQ" => SequenceParser::enter(tag, data, &mut target),
            _ => Ok(ParserState::Root),
        }
    }

    fn input(
        self,
        tag: &str,
        data: &str,
        target: &mut SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        RootParser::enter(tag, data, target)
    }
}

pub(crate) enum ParserState {
    Root,
    DatabaseReference,
    Description(DescriptionParser),
    Feature(FeatureParser),
    Literature(LiteratureParser),
    Sequence,
}

impl ParserState {
    fn parse_line(
        self,
        tag: &str,
        data: &str,
        mut target: &mut SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        match self {
            ParserState::Root => RootParser::input(RootParser, tag, data, &mut target),
            ParserState::DatabaseReference => {
                DatabaseReferenceParser::input(DatabaseReferenceParser, tag, data, &mut target)
            }
            ParserState::Description(description_parser) => {
                description_parser.input(tag, data, &mut target)
            }
            ParserState::Feature(feature_parser) => feature_parser.input(tag, data, &mut target),
            ParserState::Literature(literature_parser) => {
                literature_parser.input(tag, data, &mut target)
            }
            ParserState::Sequence => SequenceParser::input(SequenceParser, tag, data, &mut target),
        }
    }
}

impl ParseEntry for SwissEntry {
    type ParseErr = SwissParseError;
    type IterateErr = std::io::Error;

    fn parse_entry(data: &[u8]) -> Result<Self, Self::ParseErr> {
        let mut out: SwissEntry = Default::default();
        let mut mode = ParserState::Root;

        for line in BufReader::new(data).lines() {
            let line = line?;
            let tag = &line[..2];
            let data = if line.len() > 5 { &line[5..] } else { "" };

            mode = mode.parse_line(tag, data, &mut out)?;
        }

        return Ok(out);
    }

    fn make_location_iterator<F: Read + Seek>(
        file: &'_ mut F,
    ) -> Box<dyn Iterator<Item = Result<EntryLocation, <Self as ParseEntry>::IterateErr>> + '_>
    {
        Box::new(SwissEntryIterator {
            file: BufReader::new(file),
            position: 0,
        })
    }

    fn full_text_data<'a>(&'a self) -> Vec<&'a str> {
        let mut out = Vec::new();

        let process_name_category = |enc: &'a description::EntryNameCategory,
                                     out: &mut Vec<&'a str>| {
            if let Some(Evidenced { ref content, .. }) = &enc.full {
                out.push(&content[..]);
            }

            out.extend(enc.short.iter().map(|v| &v.content[..]));
            out.extend(enc.ecs.iter().map(|v| &v.content[..]));
            out.extend(enc.allergen.iter().map(|v| &v.content[..]));
            out.extend(enc.biotech.iter().map(|v| &v.content[..]));
            out.extend(enc.cd_antigens.iter().map(|v| &v.content[..]));
            out.extend(enc.inns.iter().map(|v| &v.content[..]));
        };

        let process_name = |name: &'a EntryName, out: &mut Vec<&'a str>| {
            for enc in &name.rec_name {
                process_name_category(enc, out);
            }
            for enc in &name.alt_names {
                process_name_category(enc, out);
            }
            for enc in &name.sub_names {
                process_name_category(enc, out);
            }
        };

        process_name(&self.name, &mut out);

        for name in &self.contains {
            process_name(name, &mut out);
        }

        for name in &self.includes {
            process_name(name, &mut out);
        }

        if let Some(o) = &self.source_organism {
            out.push(o);
        }

        for lr in &self.literature_references {
            if let Some(t) = &lr.title {
                out.push(t);
            }

            if let Some(g) = &lr.group {
                out.push(g);
            }

            for a in &lr.authors {
                out.push(a);
            }

            out.push(&lr.location);
        }

        out
    }

    fn parse_brief_description(
        data: &[u8],
    ) -> anyhow::Result<String, <Self as ParseEntry>::ParseErr> {
        let entry = Self::parse_entry(data)?;

        Ok(entry.name.get_preferred_name().unwrap_or_default())
    }
}

struct SwissEntryIterator<F>
where
    F: Read + Seek,
{
    file: BufReader<F>,
    position: usize,
}

impl<F> Iterator for SwissEntryIterator<F>
where
    F: Read + Seek,
{
    type Item = Result<EntryLocation, std::io::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut buf = Vec::new();
        let mut read_bytes = 0;
        let mut identifiers = Vec::new();

        if let Err(e) = self
            .file
            .seek(std::io::SeekFrom::Start(self.position as u64))
        {
            return Some(Err(e));
        }

        loop {
            buf.clear();
            match self.file.read_until('\n' as u8, &mut buf) {
                Ok(0) => break,
                Ok(n) => {
                    read_bytes += n;

                    while let Some(v) = buf.last() {
                        if *v == '\n' as u8 || *v == '\r' as u8 {
                            buf.pop();
                        } else {
                            break;
                        }
                    }

                    match &(buf[..2])[..] {
                        b"//" => break,
                        b"ID" | b"AC" => {
                            let mut start_pos = 3;
                            loop {
                                while start_pos < buf.len()
                                    && (buf[start_pos] == ' ' as u8 || buf[start_pos] == ';' as u8)
                                {
                                    start_pos += 1;
                                }

                                let mut end_pos = start_pos;
                                while end_pos < buf.len()
                                    && buf[end_pos] != ' ' as u8
                                    && buf[end_pos] != ';' as u8
                                {
                                    end_pos += 1;
                                }

                                if start_pos != end_pos {
                                    let identifier = Vec::from(&buf[start_pos..end_pos]);
                                    identifiers.push(identifier);
                                }

                                start_pos = end_pos;

                                // Only allow multiple accessions, not multiple IDs
                                if start_pos >= buf.len() || &buf[..2] == b"ID" {
                                    break;
                                }
                            }
                        }
                        _ => {}
                    }
                }
                Err(e) => return Some(Err(e)),
            }
        }

        let start = self.position;
        self.position += read_bytes;

        if read_bytes > 0 {
            Some(Ok(EntryLocation {
                identifiers,
                start,
                length: read_bytes,
            }))
        } else {
            None
        }
    }
}

impl WriteEntry for SwissEntry {
    type OutputFormat = SwissOutputFormat;
}

#[derive(Debug)]
pub enum SwissOutputFormat {
    Flat,
    Json,
    Fasta,
}

impl OutputFormat<SwissEntry> for SwissOutputFormat {
    fn write(
        &self,
        writer: &mut dyn std::io::Write,
        entries: Box<dyn Iterator<Item = (Vec<u8>, Vec<u8>)>>,
    ) -> Result<(), anyhow::Error> {
        if let SwissOutputFormat::Flat = self {
            for (_, entry) in entries {
                writer.write_all(&entry).context("writing flat entry")?;
            }
        } else {
            let entries_parsed = entries
                .map(|(id, entry)| {
                    Ok((
                        String::from_utf8_lossy(&id[..]).to_string(),
                        SwissEntry::parse_entry(&entry[..]).map_err(|e| anyhow::Error::from(e))?,
                    ))
                })
                .collect::<anyhow::Result<std::collections::HashMap<String, SwissEntry>>>()?;

            match self {
                SwissOutputFormat::Json => {
                    serde_json::ser::to_writer(writer, &entries_parsed)
                        .context("write JSON entry")?;
                }
                SwissOutputFormat::Fasta => {
                    for (k, entry) in entries_parsed {
                        write!(
                            writer,
                            ">{} {} {}\n{}\n",
                            k,
                            entry.identifier,
                            entry.name.get_preferred_name().unwrap_or_default(),
                            entry.sequence
                        )?
                    }
                }
                SwissOutputFormat::Flat => unreachable!(),
            }
        }

        Ok(())
    }

    fn content_type(&self) -> &'static str {
        match self {
            SwissOutputFormat::Flat => "text/plain",
            SwissOutputFormat::Json => "application/json",
            SwissOutputFormat::Fasta => "text/plain",
        }
    }

    fn valid_formats() -> &'static [&'static str] {
        &["flat", "json", "fasta"]
    }
}

impl std::str::FromStr for SwissOutputFormat {
    type Err = OutputFormatError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match &s.to_lowercase()[..] {
            "flat" => Ok(SwissOutputFormat::Flat),
            "json" => Ok(SwissOutputFormat::Json),
            "fasta" => Ok(SwissOutputFormat::Fasta),

            _ => Err(OutputFormatError {
                value: s.to_string(),
                supported: "flat, json, fasta".to_string(),
            }),
        }
    }
}
