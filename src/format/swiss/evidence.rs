use std::str::FromStr;

use super::SwissParseError;

/// A datum that can have evidence provided for it
#[derive(Debug, Default, PartialEq, Eq, serde::Serialize)]
pub struct Evidenced<T> {
    pub content: T,
    pub evidences: Vec<Evidence>,
}

impl<T: std::fmt::Display> Evidenced<T> {
    pub fn as_str(&self) -> String {
        format!("{}", self.content)
    }
}

impl<T> FromStr for Evidenced<T>
where
    T: FromStr,
    <T as FromStr>::Err: std::error::Error + Send + Sync + Sized + 'static,
{
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(tp) = s.find("{ECO:") {
            if let Some(ep) = s[tp..].find("}") {
                let tag_content = &s[tp + 1..tp + ep];
                let content: T = s[..tp]
                    .trim()
                    .parse()
                    .map_err(|e| SwissParseError::Boxed(Box::new(e)))?;

                let evidences: Vec<Evidence> = tag_content
                    .split(", ")
                    .map(|e: &str| e.parse())
                    .collect::<Result<Vec<_>, SwissParseError>>()?;

                return Ok(Evidenced { content, evidences });
            }
        }

        Ok(Evidenced {
            content: s.parse().map_err(|e| SwissParseError::Boxed(Box::new(e)))?,
            evidences: Vec::new(),
        })
    }
}

/// A piece of evidence
#[derive(Debug, Default, PartialEq, Eq, serde::Serialize)]
pub struct Evidence {
    /// Evidence code according to http://bioportal.bioontology.org/ontologies/ECO
    pub eco: usize,

    /// An optional external reference
    #[serde(flatten)]
    pub reference: Option<EvidenceReference>,
}

impl FromStr for Evidence {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // ECO:0000250|UniProtKB:Q8I914

        if &s[..4] != "ECO:" {
            panic!("Evidence must start with ECO:");
        }

        if let Some((eco, reference)) = s[4..].split_once("|") {
            let eco = eco.parse()?;

            let reference = Some(reference.parse()?);

            Ok(Evidence { eco, reference })
        } else {
            let eco = s[4..].parse()?;
            Ok(Evidence {
                eco,
                reference: None,
            })
        }
    }
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "type")]
pub enum EvidenceReference {
    /// A PubMed identifier
    PubMed { pubmed_id: usize },

    /// A literature reference from the entry's `literature_references` field
    LiteratureReference { number: usize },

    /// A UniProt accession number
    UniProtKb { accession_number: String },

    /// A EMBL accession number
    Embl { accession_number: String },

    /// A HAMAP-Rule accession number
    HamapRule { accession_number: String },

    /// A PDB Structure
    Pdb { accession_number: String },

    /// An evidence source that currently has no parser
    Unparsed { content: String },
}

impl FromStr for EvidenceReference {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("Ref.") {
            return Ok(EvidenceReference::LiteratureReference {
                number: s[4..].parse()?,
            });
        }

        let (tag, content) = s.split_once(":").ok_or_else(|| SwissParseError::BadState {
            context: "Parsing an evidence reference",
            data: s.to_string(),
            reason: "Expected reference to be splittable with ':'",
        })?;

        Ok(match tag {
            "PubMed" => EvidenceReference::PubMed {
                pubmed_id: content.parse()?,
            },
            "UniProtKB" => EvidenceReference::UniProtKb {
                accession_number: content.to_string(),
            },
            "EMBL" => EvidenceReference::Embl {
                accession_number: content.to_string(),
            },
            "HAMAP-Rule" => EvidenceReference::HamapRule {
                accession_number: content.to_string(),
            },
            "PDB" => EvidenceReference::Pdb {
                accession_number: content.to_string(),
            },
            _ => EvidenceReference::Unparsed {
                content: s.to_string(),
            },
        })
    }
}
