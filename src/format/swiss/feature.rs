use std::str::FromStr;

use super::{ParserState, RootParser, Subparser, SwissParseError};

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
pub struct Feature {
    /// Type of the feature
    #[serde(flatten)]
    pub feature_type: FeatureType,

    /// Sequence position of the feature
    #[serde(flatten)]
    pub position: FeaturePosition,

    /// Additional qualifiers for the feature
    pub qualifiers: Vec<FeatureQualifier>,
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "sequence_position_type")]
pub enum FeatureSequencePosition {
    Exact { position: usize },
    Beyond { position: usize },
    Unsure { position: usize },
    Unknown,
}

impl FromStr for FeatureSequencePosition {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "?" {
            Ok(FeatureSequencePosition::Unknown)
        } else if s.starts_with("<") || s.starts_with(">") {
            let position = &s[1..];
            let position: usize = position.parse()?;

            Ok(FeatureSequencePosition::Beyond { position })
        } else if s.starts_with("?") {
            let position = &s[1..];
            let position: usize = position.parse()?;

            Ok(FeatureSequencePosition::Unsure { position })
        } else {
            let position: usize = s.parse()?;

            Ok(FeatureSequencePosition::Exact { position })
        }
    }
}

/// A position specification for a sequence feature.
#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "position_type")]
pub enum FeaturePosition {
    /// A feature with a start and end position
    Range {
        isoform: Option<String>,
        start: FeatureSequencePosition,
        stop: FeatureSequencePosition,
    },

    /// A feature of a single position
    Point {
        isoform: Option<String>,
        #[serde(flatten)]
        position: FeatureSequencePosition,
    },
}

impl FromStr for FeaturePosition {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (isoform, rest) = s
            .split_once(":")
            .map(|(i, r)| (Some(i.to_string()), r))
            .unwrap_or((None, s));

        Ok(if rest.contains("..") {
            let (start, stop) = rest
                .split_once("..")
                .ok_or_else(|| SwissParseError::BadState {
                    context: "Parsing feature range",
                    data: s.to_string(),
                    reason: "Expect range to be splittable with '..'",
                })?;

            FeaturePosition::Range {
                isoform,
                start: start.parse()?,
                stop: stop.parse()?,
            }
        } else {
            FeaturePosition::Point {
                isoform,
                position: rest.parse()?,
            }
        })
    }
}

/// Additional information for a sequence feature
#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "qualifier_type")]
pub enum FeatureQualifier {
    /// A note from the 'note' qualifier
    Note { note: String },

    /// An identifier from the 'id' qualifier
    Id { id: String },

    /// Evidence from an 'evidence' qualifier
    Evidence {
        evidences: Vec<super::evidence::Evidence>,
    },

    /// Another qualifier without a parser
    NotParsed { name: String, value: String },
}

impl FromStr for FeatureQualifier {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (q_name, q_value) = s.split_once("=").ok_or_else(|| SwissParseError::BadState {
            context: "Parsing feature qualifier",
            data: s.to_string(),
            reason: "Expect qualifier to be splittable with '='",
        })?;

        // trim off quotes
        let q_value = &q_value[1..q_value.len() - 1];

        Ok(match q_name {
            "note" => FeatureQualifier::Note {
                note: q_value.to_string(),
            },
            "id" => FeatureQualifier::Id {
                id: q_value.to_string(),
            },
            "evidence" => FeatureQualifier::Evidence {
                evidences: q_value
                    .split(",")
                    .map(|e| e.trim().parse())
                    .collect::<Result<Vec<super::evidence::Evidence>, SwissParseError>>()?,
            },
            _ => FeatureQualifier::NotParsed {
                name: q_name.to_string(),
                value: q_value.to_string(),
            },
        })
    }
}

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
#[serde(tag = "feature_type")]
pub enum FeatureType {
    /// INIT_MET - Initiator methionine. Indicates that the initiator methionine has been cleaved off
    InitatorMethionine,

    /// SIGNAL - Extent of the signal prepeptide
    Signal,

    /// PROPEP - Extent of a propeptide
    Propeptide,

    /// TRANSIT - Extent of a transit peptide (mitochondrion, chloroplast, thylakoid, cyanelle,
    /// peroxisome, etc.)
    Transit,

    /// CHAIN - Extent of a polypeptide chain in the mature protein
    Chain,

    /// PEPTIDE - Extent of a released active peptide
    Peptide,

    /// TOPO_DOM - Topological domain
    TopologicalDomain,

    /// TRANSMEM - Extent of a transmembrane region
    Transmembrane,

    /// INTRAMEM - Extent of a region located in a membrane without crossing it
    Intramembrane,

    /// DOMAIN - Extent of a structural domain
    Domain,

    /// REPEAT - Extent of an internal sequence repetition
    Repeat,

    /// CA_BIND - Extent of a calcium-binding region
    CaBinding,

    /// ZN_FING - Extent of a zinc finger region
    ZnFinger,

    /// DNA_BIND - Extent of a DNA-binding region
    DnaBinding,

    /// NP_BIND - Extent of a nucleotide phosphate-binding region
    NucleotidePhosphateBinding,

    /// REGION - Extent of a region of interest
    Region,

    /// COILED - Extent of a coiled-coil region
    CoiledCoil,

    /// MOTIF - Short (up to 20aa) sequence motif of biological interest
    Motif,

    /// COMPBIAS - Extent of a compositionally biased region
    CompositionallyBiased,

    /// ACT_SITE - Amino acid(s) involved in enzyme activity
    ActiveSite,

    /// METAL - Metal binding site
    MetalBinding,

    /// BINDING - Binding site for any chemical group (co-enzyme, prosthetic group, etc.)
    Binding,

    /// SITE - Any interesting single amino-acid site not defined by another feature key
    Site,

    /// NON_STD - Non-standard amino acid
    NonStandardAminoAcid,

    /// MOD_RES - Post-translational modification of a residue
    PostTranslationalModification,

    /// LIPID - Covalent binding of lipid moiety
    Lipidated,

    /// CARBOHYD - Glycosylation site
    Glycosylated,

    /// DISULFIDE - Disulfide bond
    DisulfideBond,

    /// CROSSLNK - Posttranslationally formed amino acid bonds
    Crosslinked,

    /// VAR_SEQ - Sequence variants produced by alternative splicing, alternative promter usage,
    /// alternative initiation and ribosomal frameshifting
    AlternateSequence,

    /// VARIANT - Report that sequence variants exist
    Variant,

    /// MUTAGEN - Sites that have been experimentally altered by mutagenesis
    Mutagen,

    /// UNSURE - Uncertainties in the sequence
    SequenceUnsure,

    /// CONFLICT - Different sources report differing sequences
    SequenceConflict,

    /// NON_CONS - Non-consecutive residues
    NonConsecutiveResidues,

    /// NON_TER - Position at the extremity of the sequence that is not the terminal residue
    NonTerminalResidue,

    /// HELIX - alpha-helix secondary structure
    Helix,

    /// STRAND - beta-strand secondary structure
    Strand,

    /// TURN - H-bonded turn secondary structure
    Turn,

    NotParsed {
        #[serde(rename = "feature_type_details")]
        content: String,
    },
}

impl FromStr for FeatureType {
    type Err = SwissParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "INIT_MET" => FeatureType::InitatorMethionine,
            "SIGNAL" => FeatureType::Signal,
            "PROPEP" => FeatureType::Propeptide,
            "TRANSIT" => FeatureType::Transit,
            "CHAIN" => FeatureType::Chain,
            "PEPTIDE" => FeatureType::Peptide,
            "TOPO_DOM" => FeatureType::TopologicalDomain,
            "TRANSMEM" => FeatureType::Transmembrane,
            "INTRAMEM" => FeatureType::Intramembrane,
            "DOMAIN" => FeatureType::Domain,
            "REPEAT" => FeatureType::Repeat,
            "CA_BIND" => FeatureType::CaBinding,
            "ZN_FING" => FeatureType::ZnFinger,
            "DNA_BIND" => FeatureType::DnaBinding,
            "NP_BIND" => FeatureType::NucleotidePhosphateBinding,
            "REGION" => FeatureType::Region,
            "COILED" => FeatureType::CoiledCoil,
            "MOTIF" => FeatureType::Motif,
            "COMPBIAS" => FeatureType::CompositionallyBiased,
            "ACT_SITE" => FeatureType::ActiveSite,
            "METAL" => FeatureType::MetalBinding,
            "BINDING" => FeatureType::Binding,
            "SITE" => FeatureType::Site,
            "NON_STD" => FeatureType::NonStandardAminoAcid,
            "MOD_RES" => FeatureType::PostTranslationalModification,
            "LIPID" => FeatureType::Lipidated,
            "CARBOHYD" => FeatureType::Glycosylated,
            "DISULFIDE" => FeatureType::DisulfideBond,
            "CROSSLNK" => FeatureType::Crosslinked,
            "VAR_SEQ" => FeatureType::AlternateSequence,
            "VARIANT" => FeatureType::Variant,
            "MUTAGEN" => FeatureType::Mutagen,
            "UNSURE" => FeatureType::SequenceUnsure,
            "CONFLICT" => FeatureType::SequenceConflict,
            "NON_CONS" => FeatureType::NonConsecutiveResidues,
            "NON_TER" => FeatureType::NonTerminalResidue,
            "HELIX" => FeatureType::Helix,
            "STRAND" => FeatureType::Strand,
            "TURN" => FeatureType::Turn,
            _ => FeatureType::NotParsed {
                content: s.to_string(),
            },
        })
    }
}

pub(crate) struct FeatureParser {
    current_qualifier: Option<String>,
}

impl FeatureParser {
    fn push_qualifier(&self, target: &mut super::SwissEntry) -> Result<(), SwissParseError> {
        if let Some(ref old_qual) = self.current_qualifier {
            let oq: FeatureQualifier = old_qual.parse()?;
            target
                .features
                .last_mut()
                .ok_or_else(|| SwissParseError::BadState {
                    context: "Adding qualifier to feature",
                    data: old_qual.to_string(),
                    reason: "Could not find a feature to add to",
                })?
                .qualifiers
                .push(oq);
        }

        Ok(())
    }
}

impl Subparser for FeatureParser {
    fn enter(
        tag: &str,
        data: &str,
        target: &mut super::SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        FeatureParser {
            current_qualifier: None,
        }
        .input(tag, data, target)
    }

    fn input(
        mut self,
        tag: &str,
        data: &str,
        mut target: &mut super::SwissEntry,
    ) -> Result<ParserState, SwissParseError> {
        if tag != "FT" {
            self.push_qualifier(&mut target)?;
            return RootParser::enter(tag, data, target);
        }

        let feature_type = data[..15].trim();
        let more_data = data[16..].trim_end();

        if feature_type.len() > 0 {
            // we are starting a new feature

            // push old qualifier on old feature
            self.push_qualifier(&mut target)?;

            let feature_type: FeatureType = feature_type.parse()?;
            let position: FeaturePosition = more_data.parse()?;

            target.features.push(Feature {
                feature_type,
                position,
                qualifiers: Vec::new(),
            })
        } else {
            // we are continuing a feature

            if more_data.starts_with("/") && &more_data[..2] != "/ " {
                // push old qualifier
                self.push_qualifier(&mut target)?;

                // we are beginning a new qualifier
                let new_qual = more_data[1..].to_string();
                self.current_qualifier = Some(new_qual)
            } else {
                // we are continuing a qualifier
                self.current_qualifier
                    .as_mut()
                    .ok_or_else(|| SwissParseError::BadState {
                        context: "Extending multi-line qualifier",
                        data: more_data.to_string(),
                        reason: "Don't have a qualifier to extend",
                    })?
                    .extend(more_data.chars());
            }
        }

        Ok(ParserState::Feature(self))
    }
}
