use super::{ParserState, RootParser, Subparser, SwissParseError};

pub(crate) struct SequenceParser;

impl Subparser for SequenceParser {
    fn enter(
        _tag: &str,
        _data: &str,
        _target: &mut super::SwissEntry,
    ) -> Result<super::ParserState, SwissParseError> {
        // TODO: also parse sequence header
        Ok(ParserState::Sequence)
    }

    fn input(
        self,
        tag: &str,
        data: &str,
        mut target: &mut super::SwissEntry,
    ) -> Result<super::ParserState, SwissParseError> {
        if tag != "  " {
            return RootParser::enter(tag, data, &mut target);
        }

        target.sequence += &data.trim().replace(" ", "");

        Ok(ParserState::Sequence)
    }
}
