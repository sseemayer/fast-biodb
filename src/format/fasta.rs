use thiserror::Error;

use crate::format::{EntryLocation, ParseEntry};
use std::io::{BufRead, BufReader, Read, Seek};

use super::{GenericOutputFormat, WriteEntry};

/// A parsed FASTA entry
#[derive(Debug, Default, PartialEq, Eq, serde::Serialize)]
pub struct FastaEntry {
    /// The first part of the identifier line
    pub identifier: String,

    /// The remaining parts of the identifier line
    pub description: Option<String>,

    /// The entry sequence
    pub sequence: String,
}

#[derive(Error, Debug)]
pub enum FastaParseError {
    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error("Header does not start with '>': '{0}'")]
    BadHeader(String),
}

impl ParseEntry for FastaEntry {
    type ParseErr = FastaParseError;
    type IterateErr = std::io::Error;

    fn parse_entry(data: &[u8]) -> Result<Self, <Self as ParseEntry>::ParseErr> {
        let mut lines = BufReader::new(data).lines();

        let header: String = if let Some(l) = lines.next() {
            l?.into()
        } else {
            return Err(FastaParseError::BadHeader(String::new()));
        };

        if !header.starts_with(">") {
            return Err(FastaParseError::BadHeader(header));
        }

        let (identifier, description) = header[1..]
            .split_once(" ")
            .map(|(i, d)| (i.to_string(), Some(d.to_string())))
            .unwrap_or((header, None));

        let mut sequence = String::new();
        for line in lines {
            sequence.extend(line?.trim().replace(" ", "").chars());
        }

        Ok(FastaEntry {
            identifier,
            description,
            sequence,
        })
    }

    fn make_location_iterator<F: Read + Seek>(
        file: &mut F,
    ) -> Box<dyn Iterator<Item = Result<EntryLocation, <Self as ParseEntry>::IterateErr>> + '_>
    {
        Box::new(FastaIterator {
            file: BufReader::new(file),
            last_start: 0,
        })
    }

    fn parse_brief_description(data: &[u8]) -> Result<String, <Self as ParseEntry>::ParseErr> {
        let entry = Self::parse_entry(data)?;
        Ok(entry.description.unwrap_or(entry.identifier).to_string())
    }
}

struct FastaIterator<F: Read + Seek> {
    file: BufReader<F>,
    last_start: usize,
}

impl<'a, F: Read + Seek> Iterator for FastaIterator<F> {
    type Item = Result<EntryLocation, std::io::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut buf = Vec::new();

        let header_length = match self.file.read_until('\n' as u8, &mut buf) {
            Ok(hl) => hl,
            Err(e) => return Some(Err(e)),
        };

        if header_length == 0 {
            return None;
        }

        // UNWRAP safety: there is always at least one element in the split
        let ofs = if self.last_start == 0 { 1 } else { 0 };
        let id_length = buf
            .split(|i| *i == ' ' as u8 || *i == '\n' as u8)
            .next()
            .unwrap()
            .len();

        let identifier = buf[ofs..id_length].to_vec();

        let rest_length = match self.file.read_until('>' as u8, &mut buf) {
            Ok(rl) => rl,
            Err(e) => return Some(Err(e)),
        };

        let start = self.last_start;
        let length = header_length + rest_length - ofs;

        //println!(
        //    "'{}' @ ({}, {})",
        //    String::from_utf8_lossy(&identifier),
        //    start,
        //    length
        //);

        // expand identifiers: in addition to whole ID, tokens separated by | are identifiers
        let mut identifiers: Vec<Vec<u8>> = identifier
            .split(|i| *i == '|' as u8)
            .map(|id| id.to_vec())
            .collect();

        identifiers.insert(0, identifier);

        self.last_start = start + length;

        Some(Ok(EntryLocation {
            identifiers,
            start,
            length,
        }))
    }
}

impl WriteEntry for FastaEntry {
    type OutputFormat = GenericOutputFormat;
}
