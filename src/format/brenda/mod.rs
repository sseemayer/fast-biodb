pub mod atoms;
pub mod literature;
pub mod protein;
pub mod reaction;

use crate::format::{EntryLocation, ParseEntry};
use std::{
    collections::HashMap,
    io::{BufRead, BufReader, Read, Seek},
    str::FromStr,
};

use thiserror::Error;

use lazy_static::lazy_static;
use regex::Regex;

use crate::format::{GenericOutputFormat, WriteEntry};

use self::{
    atoms::{
        Commented as C, LiteratureReferenced as LR, ProteinReferenced as PR,
        SubstrateSpecific as SS, Value as V,
    },
    protein::{Protein, ProteinDatabaseReference},
    reaction::Reaction,
};

#[derive(Debug, Default, serde::Serialize)]
pub struct BrendaEntry {
    /// Entry identifier from the "ID" field
    pub identifier: String,

    /// Entry remark (in brackets in the ID field)
    pub remark: Option<String>,

    /// Proteins from the "PR" field
    pub proteins: HashMap<usize, LR<C<Protein, LR<PR<V<String>>>>>>,

    /// Recommended name from the "RN" field
    pub recommended_name: String,

    /// Systematic name from the "SN" field
    pub systematic_name: String,

    /// Synonyms from the "SY" field
    pub synonyms: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Reactions from the "RE" field
    pub reactions: Vec<C<Reaction, LR<PR<V<String>>>>>,

    /// Reaction types from the "RT" field
    pub reaction_types: Vec<String>,

    /// Source tissues from the "ST" field
    pub source_tissues: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Localization from the "LO" field
    pub localizations: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Natural substrates and products from the "NSP" field
    pub natural_substrate_products: Vec<LR<PR<C<Reaction, LR<PR<V<String>>>>>>>,

    /// Substrates and products from the "SP" field
    pub substrate_products: Vec<LR<PR<C<Reaction, LR<PR<V<String>>>>>>>,

    /// Turnover numbers from the "TN" field
    pub turnover_numbers: Vec<LR<PR<C<SS<V<String>>, LR<PR<V<String>>>>>>>,

    /// Km values from the "KM" field
    pub km_values: Vec<LR<PR<C<SS<V<String>>, LR<PR<V<String>>>>>>>,

    /// pH optimums from the "PHO" field
    pub ph_optimums: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// pH ranges from the "PHR" field
    pub ph_ranges: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Specific activities from the "SA" field
    pub specific_activities: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Temperature optimums from the "TO" field
    pub temperature_optimums: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Temperature ranges from the "TR" field
    pub temperature_ranges: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Cofactors from the "CF" field
    pub cofactors: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Activating compounds from the "AC" field
    pub activating_compounds: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Inhibitors from the "IN" field
    pub inhibitors: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Metals and ions from the "ME" field
    pub metals_ions: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Molecular weights from the "MW" field
    pub molecular_weights: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Posttranslational modifications from the "PM" field
    pub posttranslational_modifications: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Subunits from the "SU" field
    pub subunits: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Isoelectric points from the "PI" field
    pub isoelectric_points: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Applications from the "AP" field
    pub applications: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Engineering informations from the "EN" field
    pub engineerings: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Cloning informations from the "CL" field
    pub cloneds: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Crystallizations from the "CR" field
    pub crystallizations: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Purifications from the "PU" field
    pub purifications: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Renatured informations from the "REN" field
    pub renatureds: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// General stability informations from the "GS" field
    pub general_stabilities: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Organic solvent stability informations from the "OSS" field
    pub organic_solvent_stabilities: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Oxidation stability informations from the "OS" field
    pub oxidation_stabilities: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// pH stability informations from the "PHS" field
    pub ph_stabilities: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Storage stability informations from the "SS" field
    pub storage_stabilities: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Temperature stability informations from the "SS" field
    pub temperature_stabilities: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// Literature references from the "RF" field
    pub references: HashMap<usize, literature::LiteratureReference>,

    /// KI Values from the "KI" field
    pub ki_values: Vec<LR<PR<C<SS<V<String>>, LR<PR<V<String>>>>>>>,

    /// IC50 Values from the "IC50" field
    pub ic50_values: Vec<LR<PR<C<SS<V<String>>, LR<PR<V<String>>>>>>>,

    /// Kcat/Km Values from the "KKM" field
    pub kcat_over_km_values: Vec<LR<PR<C<SS<V<String>>, LR<PR<V<String>>>>>>>,

    /// Expression informations from the "EXP" field
    pub expressions: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,

    /// General informations from the "GI" field
    pub general_informations: Vec<LR<PR<C<V<String>, LR<PR<V<String>>>>>>>,
}

#[derive(Error, Debug)]
pub enum BrendaParseError {
    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    DateFormat(#[from] chrono::ParseError),

    #[error(transparent)]
    IntFormat(#[from] std::num::ParseIntError),

    #[error("Expected a tagged line: '{}'", _0)]
    ExpectedTag(String),

    #[error("Bad {} item: '{}", _0, _1)]
    BadItem(&'static str, String),

    #[error("State error: {}", _0)]
    BadState(String),

    #[error("Error parsing field: {}", _0)]
    Field(String),
}

/// Parse an "ID" item
fn parse_item_identifier(entry: &mut BrendaEntry, item: String) -> Result<(), BrendaParseError> {
    lazy_static! {
        static ref RE_IDENTIFIER: Regex = Regex::new(
            r"(?x)^
                (\d+\.\d+\.\d+\.\d+)    # four-part EC number
                (?:\s+\((.*)\))?        # optional remarks
                $"
        )
        .unwrap();
    }

    let captures = RE_IDENTIFIER
        .captures(&item)
        .ok_or_else(|| BrendaParseError::BadItem("ID", item.to_string()))?;

    entry.identifier = captures
        .get(1)
        .expect("Expected identifier regex group")
        .as_str()
        .to_string();
    entry.remark = captures.get(2).and_then(|c| {
        let s = c.as_str();
        if s.len() > 0 {
            Some(s.to_string())
        } else {
            None
        }
    });
    Ok(())
}

/// Parse an item that implements FromStr and can be attached to the BrendaEntry via a setter
fn parse_item_generic<T, F>(item: String, mut setter: F) -> Result<(), BrendaParseError>
where
    T: FromStr,
    <T as FromStr>::Err: std::fmt::Display,
    F: FnMut(T) -> (),
{
    let item: T = item
        .parse()
        .map_err(|e| BrendaParseError::Field(format!("Error parsing field: {}", e)))?;

    setter(item);

    Ok(())
}

/// Parse a "RN" item
fn parse_item_recommended_name(
    entry: &mut BrendaEntry,
    item: String,
) -> Result<(), BrendaParseError> {
    if !entry.recommended_name.is_empty() {
        return Err(BrendaParseError::BadState(format!(
            "Already had RN {} and refusing to overwrite with {}",
            entry.recommended_name, item
        )));
    }
    entry.recommended_name = item;
    Ok(())
}

/// Parse a "SN" item
fn parse_item_systematic_name(
    entry: &mut BrendaEntry,
    item: String,
) -> Result<(), BrendaParseError> {
    if !entry.systematic_name.is_empty() {
        return Err(BrendaParseError::BadState(format!(
            "Already had SN {} and refusing to overwrite with {}",
            entry.systematic_name, item
        )));
    }
    entry.systematic_name = item;
    Ok(())
}

fn parse_item(entry: &mut BrendaEntry, tag: String, item: String) -> Result<(), BrendaParseError> {
    match &tag[..] {
        "ID" => parse_item_identifier(entry, item),
        "PR" => protein::parse_item_protein(entry, item),
        "RN" => parse_item_recommended_name(entry, item),
        "SN" => parse_item_systematic_name(entry, item),
        "SY" => parse_item_generic(item, |i| entry.synonyms.push(i)),
        "RE" => parse_item_generic(item, |i| entry.reactions.push(i)),
        "RT" => parse_item_generic(item, |i| entry.reaction_types.push(i)),
        "ST" => parse_item_generic(item, |i| entry.source_tissues.push(i)),
        "LO" => parse_item_generic(item, |i| entry.localizations.push(i)),
        "NSP" => parse_item_generic(item, |i| entry.natural_substrate_products.push(i)),
        "SP" => parse_item_generic(item, |i| entry.substrate_products.push(i)),
        "TN" => parse_item_generic(item, |i| entry.turnover_numbers.push(i)),
        "KM" => parse_item_generic(item, |i| entry.km_values.push(i)),
        "PHO" => parse_item_generic(item, |i| entry.ph_optimums.push(i)),
        "PHR" => parse_item_generic(item, |i| entry.ph_ranges.push(i)),
        "SA" => parse_item_generic(item, |i| entry.specific_activities.push(i)),
        "TO" => parse_item_generic(item, |i| entry.temperature_optimums.push(i)),
        "TR" => parse_item_generic(item, |i| entry.temperature_ranges.push(i)),
        "CF" => parse_item_generic(item, |i| entry.cofactors.push(i)),
        "AC" => parse_item_generic(item, |i| entry.activating_compounds.push(i)),
        "IN" => parse_item_generic(item, |i| entry.inhibitors.push(i)),
        "ME" => parse_item_generic(item, |i| entry.metals_ions.push(i)),
        "MW" => parse_item_generic(item, |i| entry.molecular_weights.push(i)),
        "PM" => parse_item_generic(item, |i| entry.posttranslational_modifications.push(i)),
        "SU" => parse_item_generic(item, |i| entry.subunits.push(i)),
        "PI" => parse_item_generic(item, |i| entry.isoelectric_points.push(i)),
        "AP" => parse_item_generic(item, |i| entry.applications.push(i)),
        "EN" => parse_item_generic(item, |i| entry.engineerings.push(i)),
        "CL" => parse_item_generic(item, |i| entry.cloneds.push(i)),
        "CR" => parse_item_generic(item, |i| entry.crystallizations.push(i)),
        "PU" => parse_item_generic(item, |i| entry.purifications.push(i)),
        "REN" => parse_item_generic(item, |i| entry.renatureds.push(i)),
        "GS" => parse_item_generic(item, |i| entry.general_stabilities.push(i)),
        "OSS" => parse_item_generic(item, |i| entry.organic_solvent_stabilities.push(i)),
        "OS" => parse_item_generic(item, |i| entry.oxidation_stabilities.push(i)),
        "PHS" => parse_item_generic(item, |i| entry.ph_stabilities.push(i)),
        "SS" => parse_item_generic(item, |i| entry.storage_stabilities.push(i)),
        "TS" => parse_item_generic(item, |i| entry.temperature_stabilities.push(i)),
        "RF" => literature::parse_item_reference(entry, item),
        "KI" => parse_item_generic(item, |i| entry.ki_values.push(i)),
        "IC50" => parse_item_generic(item, |i| entry.ic50_values.push(i)),
        "KKM" => parse_item_generic(item, |i| entry.kcat_over_km_values.push(i)),
        "EXP" => parse_item_generic(item, |i| entry.expressions.push(i)),
        "GI" => parse_item_generic(item, |i| entry.general_informations.push(i)),
        _ => {
            tracing::warn!("skipping unknown tag {} '{}'", tag, item);
            Ok(())
        }
    }
}

impl ParseEntry for BrendaEntry {
    type ParseErr = BrendaParseError;
    type IterateErr = std::io::Error;

    fn parse_entry(data: &[u8]) -> Result<Self, Self::ParseErr> {
        let mut entry: BrendaEntry = Default::default();

        // iterate over lines, joining into multi-line "items" that will be added to the entry
        // via `parse_entry` method
        let mut current_tag = None;
        let mut current_item = String::new();
        for line in BufRead::split(data, b'\n') {
            let line = line?;
            if line.len() < 2 || line[0] == b'*' || line[0] == b'/' || !line.contains(&b'\t') {
                // no-content line -- skip
                continue;
            }

            let line = String::from_utf8_lossy(&line);

            let (tag, content) = line
                .split_once('\t')
                .ok_or_else(|| BrendaParseError::ExpectedTag(line.to_string()))?;

            match (&current_tag, tag) {
                (None, t) if t != "" => {
                    // new tag
                    current_tag = Some(t.to_string());
                    current_item = content.to_string();
                }
                (Some(ct), t) if t != "" => {
                    // new item or new tag

                    // store current item
                    parse_item(&mut entry, ct.to_string(), current_item)?;

                    // make fresh item
                    current_tag = Some(t.to_string());
                    current_item = content.to_string();
                }
                (Some(_ct), t) if t == "" => {
                    // append to current tag
                    current_item.push(' ');
                    current_item.extend(content.chars());
                }
                (_, _) => {}
            }
        }

        if let Some(ct) = current_tag {
            parse_item(&mut entry, ct, current_item)?;
        }

        return Ok(entry);
    }

    fn make_location_iterator<F: Read + Seek>(
        file: &'_ mut F,
    ) -> Box<dyn Iterator<Item = Result<EntryLocation, <Self as ParseEntry>::IterateErr>> + '_>
    {
        Box::new(BrendaEntryIterator {
            file: BufReader::new(file),
            position: 0,
        })
    }

    fn full_text_data<'a>(&'a self) -> Vec<&'a str> {
        let mut out: Vec<&str> = Vec::new();

        out.push(&self.identifier);

        for prot in self.proteins.values() {
            out.push(&prot.inner.inner.organism);

            for dr in prot.inner.inner.database_references.iter() {
                match dr {
                    ProteinDatabaseReference::UniProt { identifier } => out.push(&identifier),
                    ProteinDatabaseReference::GenBank { identifier } => out.push(&identifier),
                }
            }

            for c in prot.inner.comments.iter() {
                out.push(&c.inner.inner.value);
            }
        }

        out.push(&self.recommended_name);
        out.push(&self.systematic_name);

        for syn in self.synonyms.iter() {
            out.push(&syn.inner.inner.inner.value);

            for c in syn.inner.inner.comments.iter() {
                out.push(&c.inner.inner.value);
            }
        }

        for rxn in self.reactions.iter() {
            match &rxn.inner {
                Reaction::Sentence { ref sentence } => out.push(sentence),
                Reaction::Equation {
                    substrates,
                    products,
                } => {
                    for s in substrates.iter() {
                        out.push(s);
                    }

                    for p in products.iter() {
                        out.push(p);
                    }
                }
            }

            for c in rxn.comments.iter() {
                out.push(&c.inner.inner.value);
            }
        }

        out
    }

    fn parse_brief_description(
        data: &[u8],
    ) -> anyhow::Result<String, <Self as ParseEntry>::ParseErr> {
        let entry = Self::parse_entry(data)?;

        Ok(entry.recommended_name.to_string())
    }
}

struct BrendaEntryIterator<F>
where
    F: Read + Seek,
{
    file: BufReader<F>,
    position: usize,
}

impl<F> Iterator for BrendaEntryIterator<F>
where
    F: Read + Seek,
{
    type Item = Result<EntryLocation, std::io::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut buf = Vec::new();
        let mut read_bytes = 0;
        let mut identifiers = Vec::new();

        if let Err(e) = self
            .file
            .seek(std::io::SeekFrom::Start(self.position as u64))
        {
            return Some(Err(e));
        }

        loop {
            buf.clear();
            match self.file.read_until('\n' as u8, &mut buf) {
                Ok(0) => break,
                Ok(n) => {
                    read_bytes += n;

                    while let Some(v) = buf.last() {
                        if *v == '\n' as u8 || *v == '\r' as u8 {
                            buf.pop();
                        } else {
                            break;
                        }
                    }

                    if buf.len() < 2 {
                        continue;
                    }

                    match &(buf[..2])[..] {
                        b"//" => break,
                        b"ID" => {
                            let mut start_pos = 3;
                            loop {
                                while start_pos < buf.len()
                                    && (buf[start_pos] == ' ' as u8 || buf[start_pos] == ';' as u8)
                                {
                                    start_pos += 1;
                                }

                                let mut end_pos = start_pos;
                                while end_pos < buf.len()
                                    && buf[end_pos] != ' ' as u8
                                    && buf[end_pos] != ';' as u8
                                {
                                    end_pos += 1;
                                }

                                if start_pos != end_pos {
                                    let identifier = Vec::from(&buf[start_pos..end_pos]);
                                    identifiers.push(identifier);
                                }

                                start_pos = end_pos;

                                // Only allow multiple accessions, not multiple IDs
                                if start_pos >= buf.len() || &buf[..2] == b"ID" {
                                    break;
                                }
                            }
                        }
                        _ => {}
                    }
                }
                Err(e) => return Some(Err(e)),
            }
        }

        let start = self.position;
        self.position += read_bytes;

        if read_bytes > 0 {
            Some(Ok(EntryLocation {
                identifiers,
                start,
                length: read_bytes,
            }))
        } else {
            None
        }
    }
}

impl WriteEntry for BrendaEntry {
    type OutputFormat = GenericOutputFormat;
}
