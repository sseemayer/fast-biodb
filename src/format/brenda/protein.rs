use lazy_static::lazy_static;
use regex::Regex;
use std::{convert::Infallible, str::FromStr};

use serde::Serialize;

use super::{
    atoms::{Commented, LiteratureReferenced, ProteinReferenced, Value},
    BrendaEntry, BrendaParseError,
};

/// Parse a "PR" item
pub fn parse_item_protein(entry: &mut BrendaEntry, item: String) -> Result<(), BrendaParseError> {
    let pr: ProteinReferenced<
        LiteratureReferenced<
            Commented<Protein, LiteratureReferenced<ProteinReferenced<Value<String>>>>,
        >,
    > = item
        .parse()
        .map_err(|e| BrendaParseError::Field(format!("{}", e)))?;

    for prot in pr.proteins {
        entry.proteins.insert(prot, pr.inner.clone());
    }

    Ok(())
}

#[derive(Debug, Default, Serialize, Clone)]
pub struct Protein {
    /// Source organism for the protein
    pub organism: String,

    /// References to external databases
    pub database_references: Vec<ProteinDatabaseReference>,
}

#[derive(Debug, Serialize, Clone)]
#[serde(tag = "database")]
pub enum ProteinDatabaseReference {
    UniProt { identifier: String },
    GenBank { identifier: String },
}

impl FromStr for Protein {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE_PROTEIN: Regex = Regex::new(
                r"(?xi)^
                    \s*                     # whitespace
                    (.*?)                   # other content
                    \s*                     # whitespace
                    (?:
                     ([A-Z0-9_]+\s(?:UniProt|SwissProt|TrEMBL))|
                     ([A-Z0-9.]+\sGenBank)
                    )*                      # the actual references
                    \s*                     # trailing whitespace
                    $"
            )
            .unwrap();
            static ref RE_SPLIT: Regex = Regex::new(r"\s").unwrap();
        }

        let captures = RE_PROTEIN.captures(s).expect("regex should always match");

        let organism = captures
            .get(1)
            .expect("Should always capture group 1")
            .as_str()
            .to_string();

        let mut database_references = Vec::new();
        for i_capture in 1..(captures.len()) {
            if let Some(capture) = captures.get(i_capture + 1) {
                let tokens: Vec<&str> = RE_SPLIT.splitn(capture.as_str(), 2).collect();

                let (identifier, tag) = match tokens.len() {
                    1 => (tokens[0], ""),
                    2 => (tokens[0], tokens[1]),
                    _ => continue,
                };

                let identifier = identifier.to_string();

                match &tag.to_lowercase()[..] {
                    "uniprot" | "swissprot" | "trembl" => {
                        database_references.push(ProteinDatabaseReference::UniProt { identifier })
                    }

                    "genbank" => {
                        database_references.push(ProteinDatabaseReference::GenBank { identifier })
                    }

                    _ => {}
                }
            }
        }

        Ok(Protein {
            organism,
            database_references,
        })
    }
}
