use lazy_static::lazy_static;
use regex::Regex;
use serde::Serialize;

use super::{BrendaEntry, BrendaParseError};

/// A literature reference from an "RF" field
#[derive(Serialize, Debug)]
pub struct LiteratureReference {
    /// List of authors
    pub authors: Vec<String>,

    /// Title and location of the reference
    pub title_and_location: String,

    /// Pubmed ID of the reference, if applicable
    pub pubmed_id: Option<usize>,

    /// DOI of the reference, if applicable
    pub doi: Option<String>,
}

/// Parse an "RF" item
pub fn parse_item_reference(entry: &mut BrendaEntry, item: String) -> Result<(), BrendaParseError> {
    lazy_static! {
        static ref RE_REFERENCE: Regex = Regex::new(
            r"(?x)^
            <(?P<num>\d+)>                          # the numeric identifier of the reference
            \s*
            (?P<authors>[^:]+)                      # the list of authors
            :\s*
            (?P<rest>.+?)                           # generic placeholder for title and location
            (?:[.,]|\s)*
            (?:[dD][oO][iI]:?\s?(?P<doi>\S+)\.?)?   # optional: DOI
            (?:[.,]|\s)*
            (?:\{Pubmed:\s?(?P<pubmed>\d*)\})?      # optional: PubMed identifier
            (?:[.,]|\s)*
            (?:\((?P<tags>[^)]*)\))?
            (?:[.,]|\s)*
            $"
        )
        .unwrap();
        static ref RE_AUTHOR_DELIMITER: Regex = Regex::new(r";\s*").unwrap();
    }

    let captures = RE_REFERENCE
        .captures(&item)
        .ok_or_else(|| BrendaParseError::BadItem("RF", item.to_string()))?;

    let num: usize = captures
        .name("num")
        .expect("reference number")
        .as_str()
        .parse()?;

    let authors: Vec<String> = RE_AUTHOR_DELIMITER
        .split(captures.name("authors").expect("authors list").as_str())
        .map(|a| a.to_string())
        .collect();

    let title_and_location: String = captures
        .name("rest")
        .expect("Title and location")
        .as_str()
        .to_string();

    let pubmed_id: Option<usize> = if let Some(mtch) = captures.name("pubmed") {
        let mtch = mtch.as_str();

        if mtch.is_empty() {
            None
        } else {
            let pmid: usize = mtch.parse()?;
            Some(pmid)
        }
    } else {
        None
    };

    let doi: Option<String> = captures.name("doi").map(|mtch| mtch.as_str().to_string());

    entry.references.insert(
        num,
        LiteratureReference {
            authors,
            title_and_location,
            pubmed_id,
            doi,
        },
    );

    Ok(())
}
