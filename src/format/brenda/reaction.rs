use std::str::FromStr;

use serde::Serialize;
use thiserror::Error;

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum Reaction {
    Equation {
        substrates: Vec<String>,
        products: Vec<String>,
    },

    Sentence {
        sentence: String,
    },
}

#[derive(Debug, Error)]
pub enum ParseReactionError {
    #[error("Reaction without delimiter: '{}'", _0)]
    NoDelimiter(String),

    #[error("Reaction without substrates: '{}'", _0)]
    NoSubstrates(String),

    #[error("Reaction without products: '{}'", _0)]
    NoProducts(String),
}

impl FromStr for Reaction {
    type Err = ParseReactionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some((substrates, products)) = s.split_once(" = ") {
            let substrates: Vec<String> = substrates
                .split(" + ")
                .filter(|s| s.len() > 0)
                .map(|s| s.to_string())
                .collect();

            let products: Vec<String> = products
                .split(" + ")
                .filter(|s| s.len() > 0)
                .map(|s| s.to_string())
                .collect();

            if substrates.is_empty() {
                return Err(ParseReactionError::NoSubstrates(s.to_string()));
            }

            if products.is_empty() {
                return Err(ParseReactionError::NoProducts(s.to_string()));
            }

            Ok(Reaction::Equation {
                substrates,
                products,
            })
        } else {
            Ok(Reaction::Sentence {
                sentence: s.to_string(),
            })
        }
    }
}
