use lazy_static::lazy_static;
use regex::Regex;
use serde::Serialize;
use std::str::FromStr;
use thiserror::Error;

#[derive(Debug, Default, Serialize, Clone)]
pub struct LiteratureReferenced<T: Serialize + std::fmt::Debug> {
    #[serde(flatten)]
    pub inner: T,

    pub references: Vec<usize>,
}

impl<T> FromStr for LiteratureReferenced<T>
where
    T: FromStr + Serialize + std::fmt::Debug,
{
    type Err = <T as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE_REFERENCE: Regex = Regex::new(
                r"(?x)^
                    \s*                     # whitespace
                    (.*?)                   # other content
                    \s*                     # whitespace
                    (?:\{.*?\})?            # undocumented weird tags
                    \s*                     # whitespace
                    (?:<((?:[0-9,]|\s)+)>)? # the actual references
                    \s*                     # trailing whitespace
                    $"
            )
            .unwrap();
            static ref RE_SPLIT: Regex = Regex::new(r"[ ,]").unwrap();
        }

        let captures = RE_REFERENCE.captures(s).expect("regex should always match");

        let inner: T = captures
            .get(1)
            .expect("Should always capture group 1")
            .as_str()
            .trim()
            .parse()?;

        let references = if let Some(c) = captures.get(2) {
            RE_SPLIT
                .split(c.as_str())
                .map(|n| usize::from_str(n).expect("regex captures only numbers"))
                .collect()
        } else {
            Vec::new()
        };

        Ok(Self { inner, references })
    }
}

#[derive(Debug, Default, Serialize, Clone)]
pub struct ProteinReferenced<T: Serialize + std::fmt::Debug> {
    pub proteins: Vec<usize>,

    #[serde(flatten)]
    pub inner: T,
}

impl<T> FromStr for ProteinReferenced<T>
where
    T: FromStr + Serialize + std::fmt::Debug,
{
    type Err = <T as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE_REFERENCE: Regex = Regex::new(
                r"(?x)^
                    \s*                                 # beginning whitespace
                    (?:\#(?P<ref>(?:[0-9,]|\s)+)\#)?    # the actual references
                    \s*                                 # whitespace
                    (?P<rest>.*)                        # other content
                    \s*                                 # trailing whitespace
                    $"
            )
            .unwrap();
            static ref RE_SPLIT: Regex = Regex::new(r"[ ,]").unwrap();
        }

        let captures = RE_REFERENCE.captures(s).expect("regex should always match");

        let proteins = if let Some(c) = captures.name("ref") {
            RE_SPLIT
                .split(c.as_str())
                .map(|n| usize::from_str(n).expect("regex captures only numbers"))
                .collect()
        } else {
            Vec::new()
        };

        let inner: T = captures
            .name("rest")
            .expect("capture should always match")
            .as_str()
            .trim()
            .parse()?;

        Ok(Self { proteins, inner })
    }
}

#[derive(Debug, Default, Serialize, Clone)]
pub struct Commented<T: Serialize, C: Serialize> {
    #[serde(flatten)]
    pub inner: T,

    pub comments: Vec<C>,
}

#[derive(Error)]
pub enum CommentedParseError<T, C>
where
    T: FromStr + Serialize,
    <T as FromStr>::Err: std::error::Error,
    C: FromStr + Serialize,
    <C as FromStr>::Err: std::error::Error,
{
    #[error(transparent)]
    Inner(<T as FromStr>::Err),

    #[error(transparent)]
    Comment(<C as FromStr>::Err),
}

impl<T, C> FromStr for Commented<T, C>
where
    T: FromStr + Serialize,
    <T as FromStr>::Err: std::error::Error,
    C: FromStr + Serialize,
    <C as FromStr>::Err: std::error::Error,
{
    type Err = CommentedParseError<T, C>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut inner = String::new();
        let mut comments = Vec::new();
        let mut comment_buffer = String::new();

        let mut stack = Vec::new();

        let mut has_entered_comment = false;

        for (i, c) in s.chars().enumerate() {
            match c {
                '(' => {
                    has_entered_comment = true;

                    if stack.len() > 0 {
                        comment_buffer.push(c);
                    }

                    stack.push(c);
                }
                ')' => {
                    let popped = stack.pop();
                    if popped != Some('(') {
                        // we encountered unbalanced parantheses
                        // rewind and make everything we have read so far be inner content

                        inner = s.chars().take(i + 1).collect();
                        stack.clear();
                        comment_buffer.clear();
                        comments.clear();
                        has_entered_comment = false;
                        continue;
                    }

                    if stack.len() > 0 {
                        comment_buffer.push(c);
                    }

                    if stack.is_empty() && !comment_buffer.is_empty() {
                        comments.push(comment_buffer);
                        comment_buffer = String::new();
                    }
                }
                '|' => {
                    if stack.last() == Some(&'|') {
                        stack.pop();

                        if stack.is_empty() && !comment_buffer.is_empty() {
                            comments.push(comment_buffer);
                            comment_buffer = String::new();
                        }
                    } else {
                        has_entered_comment = true;
                        stack.push(c)
                    }
                }

                _ if !has_entered_comment => {
                    inner.push(c);
                }

                ';' if stack.len() == 1 => {
                    comments.push(comment_buffer);
                    comment_buffer = String::new();
                }

                _ if has_entered_comment && stack.len() > 0 => {
                    comment_buffer.push(c);
                }

                ' ' if has_entered_comment && stack.is_empty() => {}

                _ => {
                    // we encountered text after what we thought was the comment!
                    // rewind and make everything we have read so far be inner content

                    inner = s.chars().take(i + 1).collect();
                    stack.clear();
                    comment_buffer.clear();
                    comments.clear();
                    has_entered_comment = false;
                }
            }
        }

        let inner: T = inner
            .trim()
            .parse()
            .map_err(|e| CommentedParseError::Inner(e))?;

        let comments: Vec<C> = comments
            .into_iter()
            .map(|c| c.trim().parse())
            .collect::<Result<Vec<C>, <C as FromStr>::Err>>()
            .map_err(|e| CommentedParseError::Comment(e))?;

        Ok(Self { inner, comments })
    }
}

#[derive(Debug, Serialize)]
pub struct SubstrateSpecific<T>
where
    T: FromStr + Serialize,
{
    substrate: Option<String>,

    #[serde(flatten)]
    inner: T,
}

impl<T> FromStr for SubstrateSpecific<T>
where
    T: FromStr + Serialize,
{
    type Err = <T as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE_SUBSTRATE: Regex = Regex::new(
                r"(?x)^
                    \s*                 # beginning whitespace
                    (.*?)               # other content
                    \s*                 # whitespace
                    (?:\{([^}]+)\})?    # the substrate name
                    \s*                 # trailing whitespace
                    $"
            )
            .unwrap();
        }

        let captures = RE_SUBSTRATE.captures(s).expect("regex should always match");

        let inner: T = captures
            .get(1)
            .expect("Always match first group")
            .as_str()
            .trim()
            .parse()?;

        let substrate = captures.get(2).map(|c| c.as_str().to_string());

        Ok(Self { substrate, inner })
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct Value<T>
where
    T: FromStr,
{
    pub value: T,
}

impl<T> FromStr for Value<T>
where
    T: FromStr,
{
    type Err = <T as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self { value: s.parse()? })
    }
}
