use lazy_static::lazy_static;
use regex::Regex;
use serde::Serialize;

use crate::format::genbank::GenBankParseError;

use std::str::FromStr;

#[derive(Debug, Serialize)]
#[serde(tag = "type")]
pub enum Position {
    Exact { pos: usize },
    Before { max: usize },
    After { min: usize },
    Within { min: usize, max: usize },
    Between { min: usize, max: usize },
    OneOf { pos: Vec<Position> },
}

lazy_static! {
    static ref RE_POSITION_EXACT: Regex = Regex::new(r"^(\d+)$").unwrap();
    static ref RE_POSITION_BEFORE: Regex = Regex::new(r"^<(\d+)$").unwrap();
    static ref RE_POSITION_AFTER: Regex = Regex::new(r"^>(\d+)$").unwrap();
    static ref RE_POSITION_WITHIN: Regex = Regex::new(r"^(\d+)\.(\d+)$").unwrap();
    static ref RE_POSITION_BETWEEN: Regex = Regex::new(r"^(\d+)\^(\d+)$").unwrap();
    static ref RE_POSITION_ONE_OF: Regex = Regex::new(r"^one-of\((.+)\)$").unwrap();
}

impl FromStr for Position {
    type Err = GenBankParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(captures) = RE_POSITION_EXACT.captures(s) {
            let pos = captures.get(1).expect("exact pos").as_str().parse()?;
            Ok(Position::Exact { pos })
        } else if let Some(captures) = RE_POSITION_BEFORE.captures(s) {
            let max = captures.get(1).expect("max pos").as_str().parse()?;
            Ok(Position::Before { max })
        } else if let Some(captures) = RE_POSITION_AFTER.captures(s) {
            let min = captures.get(1).expect("min pos").as_str().parse()?;
            Ok(Position::After { min })
        } else if let Some(captures) = RE_POSITION_WITHIN.captures(s) {
            let min = captures.get(1).expect("min within pos").as_str().parse()?;
            let max = captures.get(2).expect("max within pos").as_str().parse()?;
            Ok(Position::Within { min, max })
        } else if let Some(captures) = RE_POSITION_BETWEEN.captures(s) {
            let min = captures.get(1).expect("min between pos").as_str().parse()?;
            let max = captures.get(2).expect("max between pos").as_str().parse()?;
            Ok(Position::Between { min, max })
        } else if let Some(captures) = RE_POSITION_ONE_OF.captures(s) {
            let pos = captures
                .get(1)
                .expect("one of pos")
                .as_str()
                .split(",")
                .map(Position::from_str)
                .collect::<Result<Vec<Position>, GenBankParseError>>()?;
            Ok(Position::OneOf { pos })
        } else {
            return Err(GenBankParseError::PositionFormat(s.to_string()));
        }
    }
}

#[derive(Debug, Serialize)]
#[serde(tag = "location_type")]
pub enum Location {
    Unspecified,
    Exact { position: Position },
    StartStop { start: Position, stop: Position },
    Composite { locations: Vec<Location> },
    Reordered { locations: Vec<Location> },
    Complement { location: Box<Location> },
}

lazy_static! {
    static ref RE_LOCATION_RANGE: Regex = Regex::new(r"^(.+)\.\.(.+)$").unwrap();
    static ref RE_LOCATION_COMPLEMENT: Regex = Regex::new(r"^complement\((.*)\)$").unwrap();
    static ref RE_LOCATION_JOIN: Regex = Regex::new(r"^join\((.*)\)$").unwrap();
    static ref RE_LOCATION_ORDER: Regex = Regex::new(r"^order\((.*)\)$").unwrap();
}

impl FromStr for Location {
    type Err = GenBankParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(captures) = RE_LOCATION_COMPLEMENT.captures(s) {
            let location = Box::new(
                captures
                    .get(1)
                    .expect("inner complement")
                    .as_str()
                    .parse()?,
            );
            Ok(Location::Complement { location })
        } else if let Some(captures) = RE_LOCATION_JOIN.captures(s) {
            let locations = captures
                .get(1)
                .expect("inner join")
                .as_str()
                .split(",")
                .map(Location::from_str)
                .collect::<Result<Vec<Location>, GenBankParseError>>()?;

            Ok(Location::Composite { locations })
        } else if let Some(captures) = RE_LOCATION_ORDER.captures(s) {
            let locations = captures
                .get(1)
                .expect("inner join")
                .as_str()
                .split(",")
                .map(Location::from_str)
                .collect::<Result<Vec<Location>, GenBankParseError>>()?;

            Ok(Location::Reordered { locations })
        } else if let Some(captures) = RE_LOCATION_RANGE.captures(s) {
            let start = captures.get(1).expect("Start pos").as_str().parse()?;
            let stop = captures.get(2).expect("Start pos").as_str().parse()?;
            Ok(Location::StartStop { start, stop })
        } else if let Ok(position) = Position::from_str(s) {
            Ok(Location::Exact { position })
        } else if s.is_empty() {
            Ok(Location::Unspecified)
        } else {
            Err(GenBankParseError::LocationFormat(s.to_string()))
        }
    }
}
