pub mod header;
pub mod location;

use crate::format::{
    genbank::{
        header::{MoleculeTopology, MoleculeType, Strandedness},
        location::{Location, Position},
    },
    EntryLocation, OutputFormat, OutputFormatError, ParseEntry, WriteEntry,
};
use chrono::NaiveDate;
use lazy_static::lazy_static;
use regex::Regex;
use serde::Serialize;
use std::{
    collections::HashMap,
    convert::TryFrom,
    io::{BufRead, BufReader, Read, Seek},
    str::FromStr,
};

use thiserror::Error;

#[derive(Debug, Default, Serialize)]
pub struct GenBankEntry {
    /// Locus name from the LOCUS field
    name: String,

    /// Sequence length from the LOCUS field
    sequence_length: usize,

    /// Molecule type from the LOCUS field
    molecule_type: Option<MoleculeType>,

    /// Strandedness from the LOCUS field
    strandedness: Strandedness,

    /// Topology from the LOCUS field
    topology: MoleculeTopology,

    /// GenBank division from the LOCUS field
    division: String,

    /// Last modification date from the LOCUS field
    last_modification_date: Option<NaiveDate>,

    /// Definition from the DEFINITION field
    definition: String,

    /// Accession numbers from the ACCESSION field
    accessions: Vec<String>,

    /// Versions from the VERSION field
    version: String,

    /// Database links from the DBLINK field
    database_links: HashMap<String, Vec<String>>,

    /// Keywords from the KEYWORDS field
    keywords: Vec<String>,

    /// Source specification from the SOURCE field
    source: Option<Source>,

    /// References from the REFERENCE fields
    references: HashMap<usize, Reference>,

    /// Features from the FEATURE field
    features: Vec<Feature>,

    /// Comments from the COMMENT field
    comments: Vec<String>,

    /// Sequence from the ORIGIN field
    sequence: String,
}

#[derive(Debug, Serialize)]
pub struct Source {
    /// Primary source specification (first line of SOURCE block)
    name: String,

    /// Source organism (first line of ORGANISM subblock)
    organism: Option<String>,

    /// Other details (further lines of ORGANISM subblock)
    organism_details: Option<String>,
}

#[derive(Error, Debug)]
pub enum GenBankParseError {
    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    DateFormat(#[from] chrono::ParseError),

    #[error(transparent)]
    IntFormat(#[from] std::num::ParseIntError),

    #[error("Malformatted header: '{}' - {}", _0, _1)]
    HeaderFormat(String, String),

    #[error("Malformatted reference: '{}'", _0)]
    ReferenceFormat(String),

    #[error("Malformatted feature line: '{}' - {}", _0, _1)]
    FeatureFormat(String, String),

    #[error("Malformatted location: '{}'", _0)]
    LocationFormat(String),

    #[error("Malformatted position: '{}'", _0)]
    PositionFormat(String),

    #[error("Malformatted sequence block: '{}' - {}", _0, _1)]
    OriginFormat(String, String),

    #[error("Encountered data after end of entry: '{}'", _0)]
    DataAfterEnd(String),

    #[error("Malformatted database link: '{}'", _0)]
    LinkFormat(String),
}

#[derive(Debug, Default, Serialize)]
pub struct Reference {
    sequence_region: Option<Location>,
    authors: Vec<String>,
    consortium: Option<String>,
    title: Option<String>,
    journal: Option<String>,
    pubmed: Option<String>,
    remark: Option<String>,
}

#[derive(Debug, Serialize)]
struct Feature {
    name: String,

    #[serde(flatten)]
    location: Location,

    qualifiers: HashMap<String, String>,
}

#[derive(Debug)]
enum SourceParserState {
    Header,
    Organism,
}

#[derive(Debug)]
enum ReferenceParserState {
    Header,
    Authors,
    Consortium,
    Title,
    Journal,
    Remark,
    PubMed,
}

#[derive(Debug)]
enum FeatureParserState {
    Header,
    Feature {
        feature_name: String,
        location: String,
        qualifiers: HashMap<String, String>,
        current_qualifier: Option<(String, String)>,
    },
}

#[derive(Debug)]
enum Field {
    Definition,
    Accession,
    Comment,
    Keywords,
}

#[derive(Debug)]
enum ParserState {
    Header,
    Entry,
    DatabaseLink,
    Field {
        field: Field,
        data: String,
    },
    Source {
        substate: SourceParserState,
    },
    Reference {
        substate: ReferenceParserState,
        ref_number: usize,
        reference: Reference,
        data: String,
    },
    Features {
        substate: FeatureParserState,
    },
    Origin,
    End,
}

const TAG_SIZE: usize = 12;
const TAG_START: &'static str = "LOCUS       ";

fn parse_locus_line_old(entry: &mut GenBankEntry, line: &str) -> Result<(), GenBankParseError> {
    // pos    content
    // 00..06 LOCUS
    // 06..12 spaces
    // 12..?? locus name
    // ??..?? space
    // ??..29 lemgth of sequence, right-justified
    // 29..33 space, bp, space
    // 33..41 strand type / molecule type, e.g. DNA
    // 41..42 space
    // 42..51 blamk(implies linear), linear or circular

    let mut name_and_length = line[TAG_SIZE..29].to_string();
    while name_and_length.contains("  ") {
        name_and_length = name_and_length.replace("  ", " ")
    }
    let name_and_length: Vec<&str> = name_and_length.split(" ").collect();
    if name_and_length.len() != 2 {
        return Err(GenBankParseError::HeaderFormat(
            line.to_string(),
            "Could not split name and length into two tokens".to_string(),
        ));
    }

    entry.name = name_and_length[0].to_string();
    entry.sequence_length = name_and_length[1].parse()?;

    entry.molecule_type = if !line[33..41].trim().is_empty() {
        Some(MoleculeType::try_from((&line[33..41], ""))?)
    } else {
        None
    };

    entry.topology = line[42..51].parse()?;

    Ok(())
}

fn parse_locus_line_new(entry: &mut GenBankEntry, line: &str) -> Result<(), GenBankParseError> {
    // pos    content
    // 00..06 LOCUS
    // 06..12 spaces
    // 12..?? locus name
    // ??..?? space
    // ??..40 length of sequence, right-justified
    // 40..44 space, bp, space
    // 44..47 blank, ss-, ds-, ms-
    // 47..54 blank, DNA, RNA, tRNA, mRNA, uRNA, cDNA
    // 54..55 space
    // 55..63 blank (implies linear), linear or circular
    // 63..64 space
    // 64..67 division code
    // 67..68 space
    // 68..79 date, formatted dd-MMM-yyyy (12-MAR-4567)

    let line = if line.len() < 79 {
        tracing::warn!("Truncated LOCUS '{}' will be padded", line);
        let mut lp = String::from(line);
        while lp.len() < 79 {
            lp.push(' ');
        }
        lp
    } else {
        line.to_string()
    };

    let mut name_and_length = line[TAG_SIZE..40].to_string();
    while name_and_length.contains("  ") {
        name_and_length = name_and_length.replace("  ", " ")
    }
    let name_and_length: Vec<&str> = name_and_length.split(" ").collect();
    if name_and_length.len() != 2 {
        return Err(GenBankParseError::HeaderFormat(
            line.to_string(),
            "Could not split name and length into two tokens".to_string(),
        ));
    }

    entry.name = name_and_length[0].to_string();
    entry.sequence_length = name_and_length[1].parse()?;

    entry.molecule_type = if !line[40..44].trim().is_empty() {
        Some(MoleculeType::try_from((&line[40..44], &line[47..54]))?)
    } else {
        None
    };

    entry.strandedness = line[44..47].parse()?;
    entry.topology = line[55..63].parse()?;

    entry.division = line[64..67].trim().to_string();

    entry.last_modification_date = if !line[68..79].trim().is_empty() {
        Some(NaiveDate::parse_from_str(&line[68..79].trim(), "%d-%b-%Y")?)
    } else {
        None
    };

    Ok(())
}

fn parse_locus_line(entry: &mut GenBankEntry, line: &str) -> Result<(), GenBankParseError> {
    // various gb locus line formats exist, try to detect the correct one
    //
    if MoleculeType::from_str(&line[29..33].trim()).is_ok() && line[55..62].trim() == "" {
        parse_locus_line_old(entry, line)
    } else if MoleculeType::from_str(line[40..44].trim()).is_ok()
        && MoleculeTopology::from_str(line[54..64].trim()).is_ok()
    {
        parse_locus_line_new(entry, line)
    } else if !line[TAG_SIZE..].trim().chars().any(|c| c == ' ') {
        // truncated LOCUS line
        todo!()
    } else {
        let tokens: Vec<&str> = line.split_whitespace().collect();

        if tokens.len() == 8
            && MoleculeType::from_str(tokens[3]).is_ok()
            && MoleculeTopology::from_str(tokens[5]).is_ok()
        {
            todo!()
        } else if tokens.len() == 7 && MoleculeType::from_str(tokens[3]).is_ok() {
            todo!()
        } else if tokens.len() >= 4 && MoleculeType::from_str(tokens[3]).is_ok() {
            todo!()
        } else if tokens.len() >= 4
            && MoleculeType::from_str(tokens.last().expect("last token")).is_ok()
        {
            todo!()
        } else {
            return Err(GenBankParseError::HeaderFormat(
                line.to_string(),
                "None of the parsers matched".to_string(),
            ));
        }
    }
}

lazy_static! {
    static ref RE_REFERENCE: Regex =
        Regex::new(r"(\d+)\s*(?:\(bases\s+(\d+)\s+to\s+(\d+)\))?").unwrap();
    static ref RE_AUTHOR_DELIM: Regex = Regex::new(r"(?:, )|(?: and )").unwrap();
    static ref RE_FEATURE_QUALIFIER: Regex = Regex::new(r#"/([^=]+)=?"?(.*?)"?$"#).unwrap();
}

impl ParserState {
    fn step(self, entry: &mut GenBankEntry, line: &str) -> Result<Self, GenBankParseError> {
        let tag_len = usize::min(TAG_SIZE, line.len());
        let tag = line[..tag_len].trim_end();
        let tag_data = &line[tag_len..];
        match self {
            ParserState::Header => {
                if !line.starts_with(TAG_START) {
                    return Ok(ParserState::Header);
                }
                parse_locus_line(entry, line)?;

                self.exit(entry)?;
                Ok(ParserState::Entry)
            }
            ParserState::Entry => match tag {
                "DEFINITION" => {
                    self.exit(entry)?;
                    Ok(ParserState::Field {
                        field: Field::Definition,
                        data: tag_data.to_string(),
                    })
                }
                "ACCESSION" => {
                    self.exit(entry)?;
                    Ok(ParserState::Field {
                        field: Field::Accession,
                        data: tag_data.to_string(),
                    })
                }
                "VERSION" => {
                    entry.version = tag_data.to_string();
                    Ok(ParserState::Entry)
                }
                "KEYWORDS" => Ok(ParserState::Field {
                    field: Field::Keywords,
                    data: tag_data.to_string(),
                }),
                "DBLINK" => {
                    let (link_type, link_data) = tag_data
                        .split_once(": ")
                        .ok_or_else(|| GenBankParseError::LinkFormat(tag_data.to_string()))?;

                    entry
                        .database_links
                        .entry(link_type.to_string())
                        .or_default()
                        .push(link_data.trim().to_string());

                    Ok(ParserState::DatabaseLink)
                }
                "SOURCE" => {
                    self.exit(entry)?;
                    entry.source = Some(Source {
                        name: tag_data.to_string(),
                        organism: None,
                        organism_details: None,
                    });
                    Ok(ParserState::Source {
                        substate: SourceParserState::Header,
                    })
                }
                "COMMENT" => {
                    self.exit(entry)?;
                    Ok(ParserState::Field {
                        field: Field::Comment,
                        data: tag_data.to_string(),
                    })
                }
                "REFERENCE" => {
                    self.exit(entry)?;

                    let m = RE_REFERENCE
                        .captures(tag_data)
                        .ok_or_else(|| GenBankParseError::ReferenceFormat(tag_data.to_string()))?;

                    let ref_number: usize = m.get(1).expect("Reference number").as_str().parse()?;

                    let sequence_region = if let (Some(start), Some(stop)) = (m.get(2), m.get(3)) {
                        let start: usize = start.as_str().parse()?;
                        let stop: usize = stop.as_str().parse()?;
                        Some(Location::StartStop {
                            start: Position::Exact { pos: start },
                            stop: Position::Exact { pos: stop },
                        })
                    } else {
                        None
                    };

                    let reference = Reference {
                        sequence_region,
                        ..Default::default()
                    };

                    Ok(ParserState::Reference {
                        substate: ReferenceParserState::Header,
                        ref_number,
                        reference,
                        data: String::new(),
                    })
                }
                "FEATURES" => {
                    self.exit(entry)?;
                    Ok(ParserState::Features {
                        substate: FeatureParserState::Header,
                    })
                }
                "ORIGIN" => {
                    self.exit(entry)?;
                    Ok(ParserState::Origin)
                }
                _ => {
                    tracing::warn!("Unknown field: '{}'", line);
                    Ok(ParserState::Entry)
                }
            },
            ParserState::DatabaseLink => {
                if tag.is_empty() {
                    let (link_type, link_data) = tag_data
                        .split_once(": ")
                        .ok_or_else(|| GenBankParseError::LinkFormat(tag_data.to_string()))?;

                    entry
                        .database_links
                        .entry(link_type.to_string())
                        .or_default()
                        .push(link_data.trim().to_string());

                    Ok(ParserState::DatabaseLink)
                } else {
                    self.exit(entry)?;
                    ParserState::Entry.step(entry, line)
                }
            }
            ParserState::Field { field, mut data } => {
                if line[..tag_len].trim_end() == "" {
                    data.push(' ');
                    data.extend(line[TAG_SIZE..].chars());
                    Ok(ParserState::Field { field, data })
                } else {
                    ParserState::Field { field, data }.exit(entry)?;
                    ParserState::Entry.step(entry, line)
                }
            }
            ParserState::Source { substate } => {
                let source = entry.source.as_mut().expect("source");

                if tag.is_empty() {
                    // continuation of field
                    match substate {
                        SourceParserState::Header => {
                            source.name.push(' ');
                            source.name.extend(tag_data.chars());
                            Ok(ParserState::Source {
                                substate: SourceParserState::Header,
                            })
                        }
                        SourceParserState::Organism => {
                            if let Some(od) = source.organism_details.as_mut() {
                                od.push(' ');
                                od.extend(tag_data.chars());
                            } else {
                                source.organism_details = Some(tag_data.to_string())
                            }

                            Ok(ParserState::Source {
                                substate: SourceParserState::Organism,
                            })
                        }
                    }
                } else if line[..2].trim_end().is_empty() {
                    // new sub field
                    let sub_tag = line[2..tag_len].trim_end();
                    match sub_tag {
                        "ORGANISM" => {
                            source.organism = Some(tag_data.to_string());
                            Ok(ParserState::Source {
                                substate: SourceParserState::Organism,
                            })
                        }
                        _ => {
                            tracing::warn!("Unknown source subtag {}", sub_tag);
                            Ok(ParserState::Source {
                                substate: SourceParserState::Header,
                            })
                        }
                    }
                } else {
                    // new field
                    ParserState::Source { substate }.exit(entry)?;
                    ParserState::Entry.step(entry, line)
                }
            }
            ParserState::Reference {
                substate,
                ref_number,
                mut reference,
                mut data,
            } => {
                if tag.is_empty() {
                    // continuation of field
                    data.push(' ');
                    data.extend(line[TAG_SIZE..].chars());
                    Ok(ParserState::Reference {
                        substate,
                        ref_number,
                        reference,
                        data,
                    })
                } else if line[..2].trim_end() == "" {
                    // new sub field

                    // close prior sub field
                    match substate {
                        ReferenceParserState::Header => {}
                        ReferenceParserState::Authors => reference
                            .authors
                            .extend(RE_AUTHOR_DELIM.split(&data).map(|a| a.to_string())),
                        ReferenceParserState::Consortium => reference.consortium = Some(data),
                        ReferenceParserState::Title => reference.title = Some(data),
                        ReferenceParserState::Journal => reference.journal = Some(data),
                        ReferenceParserState::Remark => reference.remark = Some(data),
                        ReferenceParserState::PubMed => reference.pubmed = Some(data),
                    }

                    let data = tag_data.to_string();

                    let sub_tag = line[2..tag_len].trim();
                    let substate = match sub_tag {
                        "AUTHORS" => ReferenceParserState::Authors,
                        "CONSRTM" => ReferenceParserState::Consortium,
                        "TITLE" => ReferenceParserState::Title,
                        "JOURNAL" => ReferenceParserState::Journal,
                        "PUBMED" => ReferenceParserState::PubMed,
                        "REMARK" => ReferenceParserState::Remark,
                        _ => {
                            tracing::warn!("Unknown reference subtag {}", sub_tag);
                            ReferenceParserState::Header
                        }
                    };

                    Ok(ParserState::Reference {
                        substate,
                        ref_number,
                        reference,
                        data,
                    })
                } else {
                    ParserState::Reference {
                        substate,
                        ref_number,
                        reference,
                        data,
                    }
                    .exit(entry)?;
                    ParserState::Entry.step(entry, line)
                }
            }
            ParserState::Features { substate } => {
                match substate {
                    FeatureParserState::Header => {
                        if !tag.is_empty() && !tag.starts_with("  ") {
                            return ParserState::Entry.step(entry, line);
                        }

                        let longtag_length = usize::min(line.len(), 21);
                        let longtag = line[..longtag_length].trim_end();
                        let longtag_data = &line[longtag_length..];

                        if longtag.len() <= 5 {
                            return Err(GenBankParseError::FeatureFormat(
                                line.to_string(),
                                "Expected feature type".to_string(),
                            ));
                        };
                        let feature_name = longtag[5..].to_string();
                        let location = longtag_data.to_string();
                        let qualifiers = HashMap::new();

                        Ok(ParserState::Features {
                            substate: FeatureParserState::Feature {
                                feature_name,
                                location,
                                qualifiers,
                                current_qualifier: None,
                            },
                        })
                    }

                    FeatureParserState::Feature {
                        feature_name,
                        mut location,
                        mut qualifiers,
                        current_qualifier,
                    } => {
                        let longtag_length = usize::min(line.len(), 21);
                        let longtag = line[..longtag_length].trim_end();
                        let longtag_data = &line[longtag_length..];

                        if !longtag.is_empty() {
                            ParserState::Features {
                                substate: FeatureParserState::Feature {
                                    feature_name,
                                    location,
                                    qualifiers,
                                    current_qualifier,
                                },
                            }
                            .exit(entry)?;

                            if longtag.starts_with(' ') && longtag.len() > 5 {
                                return ParserState::Features {
                                    substate: FeatureParserState::Header,
                                }
                                .step(entry, line);
                            } else {
                                // back to the root
                                return ParserState::Entry.step(entry, line);
                            }
                        }

                        if let Some(captures) = RE_FEATURE_QUALIFIER.captures(longtag_data) {
                            if let Some((cq_name, cq_data)) = current_qualifier {
                                // close last qualifier
                                qualifiers.insert(cq_name, cq_data);
                            }

                            let cq_name = captures
                                .get(1)
                                .expect("Qualifier name")
                                .as_str()
                                .to_string();

                            let cq_data = captures
                                .get(2)
                                .expect("Qualifier data")
                                .as_str()
                                .to_string();

                            Ok(ParserState::Features {
                                substate: FeatureParserState::Feature {
                                    feature_name,
                                    location,
                                    qualifiers,
                                    current_qualifier: Some((cq_name, cq_data)),
                                },
                            })
                        } else if let Some((cq_name, mut cq_data)) = current_qualifier {
                            cq_data.extend(longtag_data.chars());

                            if longtag_data.ends_with('"') {
                                cq_data.pop();
                            }
                            Ok(ParserState::Features {
                                substate: FeatureParserState::Feature {
                                    feature_name,
                                    location,
                                    qualifiers,
                                    current_qualifier: Some((cq_name, cq_data)),
                                },
                            })
                        } else {
                            location.extend(longtag_data.chars());
                            Ok(ParserState::Features {
                                substate: FeatureParserState::Feature {
                                    feature_name,
                                    location,
                                    qualifiers,
                                    current_qualifier: None,
                                },
                            })
                        }
                    }
                }
            }
            ParserState::Origin => {
                if tag == "//" {
                    self.exit(entry)?;
                    return Ok(ParserState::End);
                }

                if line.len() < 11 {
                    return Err(GenBankParseError::OriginFormat(
                        line.to_string(),
                        "Sequence line too short".to_string(),
                    ));
                }

                let sequence_pos: usize = line[..10].trim().parse()?;
                if sequence_pos - 1 != entry.sequence.len() {
                    return Err(GenBankParseError::OriginFormat(
                        line.to_string(),
                        format!(
                            "Got sequence position {} but expected {}",
                            sequence_pos,
                            entry.sequence.len() + 1
                        ),
                    ));
                }

                let sequence_chunk = line[10..].replace(' ', "");
                entry.sequence.extend(sequence_chunk.chars());

                Ok(ParserState::Origin)
            }
            ParserState::End => return Err(GenBankParseError::DataAfterEnd(line.to_string())),
        }
    }

    fn exit(self, entry: &mut GenBankEntry) -> Result<(), GenBankParseError> {
        match self {
            ParserState::Header => {}
            ParserState::Entry => {}
            ParserState::DatabaseLink => {}
            ParserState::Field { field, data } => match field {
                Field::Definition => entry.definition = data.to_string(),
                Field::Accession => entry
                    .accessions
                    .extend(data.split_whitespace().map(|a| a.to_string())),
                Field::Comment => entry.comments.push(data),
                Field::Keywords => entry.keywords.extend(
                    data.trim_end_matches('.')
                        .split("; ")
                        .filter(|v| !v.is_empty())
                        .map(|v| v.to_string()),
                ),
            },
            ParserState::Source { substate: _ } => {}
            ParserState::Reference {
                substate,
                ref_number,
                mut reference,
                data,
            } => {
                // close prior sub field
                match substate {
                    ReferenceParserState::Header => {}
                    ReferenceParserState::Authors => reference
                        .authors
                        .extend(RE_REFERENCE.split(&data).map(|a| a.to_string())),
                    ReferenceParserState::Consortium => reference.consortium = Some(data),
                    ReferenceParserState::Title => reference.title = Some(data),
                    ReferenceParserState::Journal => reference.journal = Some(data),
                    ReferenceParserState::Remark => reference.remark = Some(data),
                    ReferenceParserState::PubMed => reference.pubmed = Some(data),
                }

                entry.references.insert(ref_number, reference);
            }
            ParserState::Features { substate } => match substate {
                FeatureParserState::Header => {}
                FeatureParserState::Feature {
                    feature_name,
                    location,
                    qualifiers,
                    current_qualifier,
                } => {
                    if feature_name.is_empty() && location.is_empty() {
                        return Ok(());
                    }

                    let location: Location = location.parse()?;

                    let mut feature = Feature {
                        name: feature_name,
                        location,
                        qualifiers,
                    };

                    if let Some((cq_name, cq_data)) = current_qualifier {
                        feature.qualifiers.insert(cq_name, cq_data);
                    }

                    entry.features.push(feature);
                }
            },
            ParserState::Origin => {}
            ParserState::End => {}
        }
        Ok(())
    }
}

impl ParseEntry for GenBankEntry {
    type ParseErr = GenBankParseError;
    type IterateErr = std::io::Error;

    fn parse_entry(data: &[u8]) -> std::result::Result<Self, <Self as ParseEntry>::ParseErr> {
        let reader = BufReader::new(data);
        let mut entry = GenBankEntry::default();
        let mut state = ParserState::Header;

        for line in reader.lines() {
            let line = line?;
            state = state.step(&mut entry, &line[..])?;
        }

        Ok(entry)
    }

    fn make_location_iterator<F: Read + Seek>(
        file: &mut F,
    ) -> Box<
        dyn Iterator<Item = std::result::Result<EntryLocation, <Self as ParseEntry>::IterateErr>>
            + '_,
    > {
        Box::new(GenBankEntryIterator {
            file: BufReader::new(file),
            position: 0,
        })
    }

    fn full_text_data(&self) -> Vec<&str> {
        let mut out = Vec::new();

        out.push(&self.name[..]);
        out.push(&self.definition[..]);
        out.push(&self.version[..]);
        out.extend(self.accessions.iter().map(|a| &a[..]));
        out.extend(self.comments.iter().map(|c| &c[..]));
        out.extend(self.database_links.values().flatten().map(|l| &l[..]));
        out.extend(self.keywords.iter().map(|k| &k[..]));

        if let Some(s) = &self.source {
            out.push(&s.name);
            out.extend(s.organism.iter().map(|v| &v[..]));
            out.extend(s.organism_details.iter().map(|v| &v[..]));
        }

        for reference in self.references.values() {
            out.extend(reference.title.iter().map(|v| &v[..]));
            out.extend(reference.authors.iter().map(|v| &v[..]));
            out.extend(reference.consortium.iter().map(|v| &v[..]));
            out.extend(reference.title.iter().map(|v| &v[..]));
            out.extend(reference.pubmed.iter().map(|v| &v[..]));
            out.extend(reference.remark.iter().map(|v| &v[..]));
        }

        out
    }

    fn parse_brief_description(
        data: &[u8],
    ) -> std::result::Result<String, <Self as ParseEntry>::ParseErr> {
        // TODO more efficient parser
        let entry = Self::parse_entry(data)?;
        Ok(entry.definition.to_string())
    }
}

struct GenBankEntryIterator<F>
where
    F: Read + Seek,
{
    file: BufReader<F>,
    position: usize,
}

impl<F> Iterator for GenBankEntryIterator<F>
where
    F: Read + Seek,
{
    type Item = Result<EntryLocation, std::io::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut buf = Vec::new();
        let mut read_bytes = 0;
        let mut identifiers = Vec::new();

        let mut start = None;

        loop {
            buf.clear();
            match self.file.read_until(b'\n', &mut buf) {
                Ok(0) => break,
                Ok(n) => {
                    read_bytes += n;

                    while let Some(v) = buf.last() {
                        if *v == b'\n' || *v == b'\r' {
                            buf.pop();
                        } else {
                            break;
                        }
                    }

                    if buf.starts_with(b"LOCUS       ") {
                        start = Some(self.position + read_bytes - n);

                        let (identifier_pos, _) = buf[12..]
                            .iter()
                            .enumerate()
                            .find(|(_i, &a)| a == b' ')
                            .unwrap_or((buf.len() - 12, &b' '));

                        let identifier = &buf[12..(12 + identifier_pos)];
                        identifiers.push(identifier.to_vec());
                    } else if buf.starts_with(b"VERSION") {
                        let identifier = buf[12..].to_vec();
                        identifiers.push(identifier);
                    } else if buf.starts_with(b"//") {
                        break;
                    }
                }
                Err(e) => return Some(Err(e)),
            }
        }

        self.position += read_bytes;

        if read_bytes > 0 && start.is_some() {
            let start = start.expect("checked start to exist before");
            Some(Ok(EntryLocation {
                identifiers,
                start,
                length: self.position - start,
            }))
        } else {
            None
        }
    }
}

impl WriteEntry for GenBankEntry {
    type OutputFormat = GenBankOutputFormat;
}

#[derive(Debug)]
pub enum GenBankOutputFormat {
    Flat,
    Json,
    Fasta,
}

impl OutputFormat<GenBankEntry> for GenBankOutputFormat {
    fn write(
        &self,
        writer: &mut dyn std::io::Write,
        entries: Box<dyn Iterator<Item = (Vec<u8>, Vec<u8>)>>,
    ) -> anyhow::Result<()> {
        match self {
            GenBankOutputFormat::Flat => {
                for (_, entry) in entries {
                    writer.write_all(&entry)?;
                }
                Ok(())
            }
            GenBankOutputFormat::Json => {
                let entries_parsed = entries
                    .map(|(id, entry)| {
                        Ok((
                            String::from_utf8_lossy(&id[..]).to_string(),
                            GenBankEntry::parse_entry(&entry[..])?,
                        ))
                    })
                    .collect::<anyhow::Result<HashMap<String, GenBankEntry>>>()?;

                serde_json::ser::to_writer(writer, &entries_parsed)?;

                Ok(())
            }
            GenBankOutputFormat::Fasta => {
                for (id, entry) in entries {
                    let id = String::from_utf8_lossy(&id[..]);
                    let entry = GenBankEntry::parse_entry(&entry[..])?;

                    write!(
                        writer,
                        ">{} {} {}\n{}\n",
                        id, &entry.name, &entry.definition, &entry.sequence
                    )?;
                }

                Ok(())
            }
        }
    }

    fn content_type(&self) -> &'static str {
        match self {
            GenBankOutputFormat::Flat => "text/plain",
            GenBankOutputFormat::Json => "application/json",
            GenBankOutputFormat::Fasta => "text/plain",
        }
    }

    fn valid_formats() -> &'static [&'static str] {
        &["flat", "json", "fasta"]
    }
}

impl FromStr for GenBankOutputFormat {
    type Err = OutputFormatError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match &s.to_lowercase()[..] {
            "flat" => Ok(GenBankOutputFormat::Flat),
            "json" => Ok(GenBankOutputFormat::Json),
            "fasta" => Ok(GenBankOutputFormat::Fasta),
            _ => Err(OutputFormatError {
                value: s.to_string(),
                supported: "flat, json, fasta".to_string(),
            }),
        }
    }
}
