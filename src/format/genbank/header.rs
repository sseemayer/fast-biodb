use crate::format::genbank::GenBankParseError;
use serde::Serialize;

use std::str::FromStr;

#[derive(Debug, Serialize)]
pub enum MoleculeType {
    Dna,
    Rna,
    Protein,
    Rc,
}

impl FromStr for MoleculeType {
    type Err = GenBankParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        MoleculeType::try_from((s, ""))
    }
}

impl TryFrom<(&str, &str)> for MoleculeType {
    type Error = GenBankParseError;

    fn try_from(v: (&str, &str)) -> Result<Self, GenBankParseError> {
        match (v.0.trim(), v.1.trim()) {
            ("bp", "") => Ok(MoleculeType::Dna),
            ("bp", _) if v.1.trim().ends_with("DNA") => Ok(MoleculeType::Dna),
            ("bp", _) if v.1.trim().ends_with("RNA") => Ok(MoleculeType::Rna),
            ("aa", "") => Ok(MoleculeType::Protein),
            ("rc", _) => Ok(MoleculeType::Rc),
            _ => Err(GenBankParseError::HeaderFormat(
                format!("{} / {}", v.0, v.1),
                "Unrecognized molecule type".to_string(),
            )),
        }
    }
}

#[derive(Debug, Serialize)]
pub enum MoleculeTopology {
    Linear,
    Circular,
}

impl FromStr for MoleculeTopology {
    type Err = GenBankParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.trim() {
            "linear" | "" => Ok(MoleculeTopology::Linear),
            "circular" => Ok(MoleculeTopology::Circular),
            _ => Err(GenBankParseError::HeaderFormat(
                s.to_string(),
                "Unrecognized topology".to_string(),
            )),
        }
    }
}

impl Default for MoleculeTopology {
    fn default() -> Self {
        MoleculeTopology::Linear
    }
}

#[derive(Debug, Serialize)]
pub enum Strandedness {
    Undefined,
    Single,
    Double,
    Ms,
}

impl FromStr for Strandedness {
    type Err = GenBankParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.trim() {
            "" => Ok(Strandedness::Undefined),
            "ss-" => Ok(Strandedness::Single),
            "ds-" => Ok(Strandedness::Double),
            "ms-" => Ok(Strandedness::Ms),
            _ => Err(GenBankParseError::HeaderFormat(
                s.to_string(),
                "Unrecognized strandedness".to_string(),
            )),
        }
    }
}

impl Default for Strandedness {
    fn default() -> Self {
        Strandedness::Undefined
    }
}
