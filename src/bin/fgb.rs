use anyhow::Result;

use fast_biodb::{cli::Cli, format::genbank::GenBankEntry};

fn main() -> Result<()> {
    Cli::<GenBankEntry>::main()
}
