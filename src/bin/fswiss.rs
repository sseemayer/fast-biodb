use anyhow::Result;

use fast_biodb::{cli::Cli, format::swiss::SwissEntry};

fn main() -> Result<()> {
    Cli::<SwissEntry>::main()
}
