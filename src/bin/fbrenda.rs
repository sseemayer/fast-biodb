use anyhow::Result;

use fast_biodb::{cli::Cli, format::brenda::BrendaEntry};

fn main() -> Result<()> {
    Cli::<BrendaEntry>::main()
}
