use anyhow::Result;

use fast_biodb::{cli::Cli, format::fasta::FastaEntry};

fn main() -> Result<()> {
    Cli::<FastaEntry>::main()
}
