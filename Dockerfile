FROM ghcr.io/rust-cross/rust-musl-cross:x86_64-musl as builder

ADD ./ /

RUN cargo build \
	--release \
	--target x86_64-unknown-linux-musl

FROM scratch

COPY --from=builder /target/x86_64-unknown-linux-musl/* /
