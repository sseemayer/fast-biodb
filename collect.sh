#!/bin/bash
set +e
shopt -s extglob

target_folder="${1:-target/release}"

version=$(awk '/^version/ {print $3}' < Cargo.toml)
version="${version%\"}"
version="${version#\"}"

folder="fast-biodb-${version}"

echo "Collecting v${version} from ${target_folder} to ${folder}"

rm -rf $folder
mkdir -p $folder
cp -v ${target_folder}/f+([a-z]) ${folder}/

