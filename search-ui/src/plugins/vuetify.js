// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'

const darkTheme = {
  dark: true,
  colors: {
    background: '#000000',
    surface: '#222222',
    primary: '#008b00',
    secondary: '#6B6442',
    error: '#B00020',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FB8C00',
  }
}

const lightTheme = {
  dark: false,
  colors: {
    background: '#ffffff',
    surface: '#f5f5f5',
    primary: '#9AE69A',
    secondary: '#FCEA9D',
    error: '#B00020',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FB8C00',
  }
}

export default createVuetify({
  // https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
  theme: {
    defaultTheme: "lightTheme",
    themes: {
      darkTheme,
      lightTheme,
    }
  }
})
