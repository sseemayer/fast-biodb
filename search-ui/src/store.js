import axios from 'axios'
import { defineStore } from 'pinia'

export const useStore = defineStore('main', {
  state: () => {
    return {
      query: '',
      limit: 10,
      offset: 0,

      hits: 0,
      total: 0,

      theme: 'lightTheme',

      config: {
        programName: "fast-biodb",
        version: "0.0.0",
        outputFormats: [],
      }
    }
  },

  actions: {
    async search() {
      let res = await axios.post("api/search", {
        query: this.query,
        limit: this.limit,
        offset: this.offset,
      })

      this.hits = res.data.hits
      this.total = res.data.total
    },

    async loadConfig() {
      let res = await axios.get("config")
      this.config = res.data
    },
  }

})

