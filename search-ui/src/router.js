import { createRouter, createWebHashHistory } from 'vue-router'

import Home from '@/views/Home.vue'
import Details from '@/views/Details.vue'

const routes = [
  {
    name: 'home',
    path: '/',
    component: Home
  },
  {
    name: 'details',
    path: '/details/:identifier/:view',
    component: Details,
    props: true
  },
]

export const router = createRouter({
    history: createWebHashHistory(),
    routes,
})
