import { createApp } from 'vue'
import { createPinia } from 'pinia'

import { router } from './router'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'

import JsonViewer from 'vue3-json-viewer'
import "vue3-json-viewer/dist/index.css"

import App from './App.vue'

loadFonts()

const pinia = createPinia()

createApp(App)
  .use(pinia)
  .use(vuetify)
  .use(router)
  .use(JsonViewer)
  .mount('#app')
