# fast-biodb
Quickly access biological database flatfiles via an added-on search index, powered by [Tantivy](https://github.com/quickwit-oss/tantivy)

Provides both a Rust library with parsers, and command line applications `fbrenda`, `ffasta`, `fgb` and `fswiss` for accessing the respective formats

## Usage

### Index Creation
First, you need to create a database index:
```bash
# Create a database index for a SwissProt flatfile database (including full-text indices)
fswiss index -d uniprot_sprot.dat.idx uniprot_sprot.dat --full-text
```

The `uniprot_sprot.dat.idx` argument is the name of the index to create and can be chosen arbitrarily. You can also add more than one flatfile database to e.g. create a joint index for `uniprot_sprot.dat` and `uniprot_trembl.dat`.

Alternatively, you can restrict to only indexing record identifiers by omitting the `--full-text` flag.

### Querying

**Once `uniprot_sprot.dat.idx` exists**, you can access data from it in several ways:
```bash
# Get an entry from SwissProt (flatfile format)
fswiss get -d uniprot_sprot.dat.idx AMY1A_HUMAN

# Get an entry from SwissProt (FASTA format)
fswiss get -d uniprot_sprot.dat.idx AMY1A_HUMAN -f fasta

# Get an entry from SwissProt (JSON format)
fswiss get -d uniprot_sprot.dat.idx AMY1A_HUMAN -f json

# Perform full-text search (requires a database index with --full-text flag)
fswiss search -d uniprot_sprot.dat.idx "+kinase +inhibitor"
```

### Built-in web server

If you have compiled `fast-biodb` with the `server` feature flag, you can additionally use the following command to open up a webserver listening on [localhost:3000](http://localhost:3000) by default. 

```bash
fswiss serve -d uniprot_sprot.dat.idx
```

This provides a user-friendly search UI at [/](http://localhost:3000/), and a Swagger-documented REST API at [/api/swagger](http://localhost:3000/api/swagger).

## Download and Installation

### Precompiled
See the [Releases page](https://gitlab.com/sseemayer/fast-biodb/-/releases) for statically-compiled binaries.

### From Source
You can compile yourself using `cargo build --release` using a *nightly* rust version.

## License
MIT
